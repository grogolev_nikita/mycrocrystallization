import cv2
import tkinter as tk
import numpy as np
import scipy as sp
import imutils

from math import pi
from tkinter import filedialog
from skimage import (img_as_ubyte, io, measure)
from skimage.filters import threshold_sauvola
from skimage.filters.rank import entropy
from skimage.morphology import disk
from imutils import contours

def openFile():
    root = tk.Tk()
    root.withdraw()

    file_path = filedialog.askopenfilename()
    return file_path

def open_window(name, img, width=800, height=600):
    cv2.namedWindow(name, cv2.WINDOW_NORMAL)
    cv2.resizeWindow(name, width, height)
    cv2.imshow(name, img)

def detect_blobs(img, original=None):
    labels = measure.label(img, neighbors=8, background=0)
    mask = np.zeros(img.shape, dtype="uint8")
    
    # loop over the unique components
    for label in np.unique(labels):
        # if this is the background label, ignore it
        if label == 0:
            continue
     
        # otherwise, construct the label mask and count the
        # number of pixels 
        labelMask = np.zeros(img.shape, dtype="uint8")
        labelMask[labels == label] = 255
        numPixels = cv2.countNonZero(labelMask)
     
        # if the number of pixels in the component is sufficiently
        # large, then add it to our mask of "large blobs"
        if numPixels > 30:
            mask = cv2.add(mask, labelMask)
    
    cnts = cv2.findContours(mask.copy(), cv2.RETR_EXTERNAL,cv2.CHAIN_APPROX_SIMPLE)
    cnts = cnts[0] if imutils.is_cv2() else cnts[1]
    cnts = contours.sort_contours(cnts)[0]
    
    totalBlobs = 0
    for (i, c) in enumerate(cnts):
        (x, y, w, h) = cv2.boundingRect(c)
        subImg = img[y:y+h, x:x+w]
        rectPxArea = w * h
        whitePxCount = cv2.countNonZero(subImg)
        whitePxDencity = whitePxCount / rectPxArea
        ((cX, cY), radius) = cv2.minEnclosingCircle(c)
        
        #for dark crystals
        if ((w / h > 3) or (h / w > 3)):
            continue
        else:
            if (whitePxDencity < 0.3):
                continue
        
        if (rectPxArea < 1000):
            continue
            
        if (radius < 50):
            continue
        
        totalBlobs += 1
        
        if (original is None):
            continue
            
        cv2.circle(original, (int(cX), int(cY)), int(radius),
            (0, 0, 255), 3)
        cv2.putText(original, "#{}".format(i + 1), (x, y - 15),
            cv2.FONT_HERSHEY_SIMPLEX, 0.45, (0, 0, 255), 2)
            
    print('Total blobs: ', totalBlobs)

def preprocess_image():
    original = cv2.imread(openFile(), 0)
    
    img = original
    img = img_as_ubyte(img)
    img = cv2.bilateralFilter(img,25,75,75)


    hist, bins = np.histogram(img.flatten(), 256, [0,256])
    cdf = hist.cumsum()
    cdf_normalized = cdf * hist.max() / cdf.max()
    cdf_m = np.ma.masked_equal(cdf,0)
    cdf_m = (cdf_m - cdf_m.min())*255/(cdf_m.max()-cdf_m.min())
    cdf = np.ma.filled(cdf_m,0).astype('uint8')
    img = cdf[img]

    open_window('hist', img)
    thresh = cv2.threshold(img, 10, 255, cv2.THRESH_BINARY_INV)[1]
    kernel = np.ones((5,5),np.uint8)
    thresh = cv2.erode(thresh,kernel,iterations = 2)
    
    #kernel = np.ones((81,81),np.uint8)
    #thresh = cv2.morphologyEx(thresh, cv2.MORPH_CLOSE, kernel)
    
    open_window('thresh', thresh)
    detect_blobs(thresh, original)
    
    open_window('blobs', original)
    cv2.waitKey(0)
    
def main():    
    preprocess_image()
    
if __name__ == '__main__':
    main()
