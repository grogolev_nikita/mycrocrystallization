import cv2
import tkinter as tk
import numpy as np
import scipy as sp
import math
import argparse
import os
from skimage.filters.rank import entropy

from tkinter import filedialog
from skimage import img_as_ubyte
from skimage import io
from skimage.filters import (threshold_otsu, threshold_niblack,
                             threshold_sauvola)
from skimage.filters.rank import entropy
from skimage.morphology import diamond

def choose_file():
    root = tk.Tk()
    root.withdraw()

    file_path = filedialog.askopenfilename()
    return file_path

def open_window(name, img, width=800, height=600):
    cv2.namedWindow(name, cv2.WINDOW_NORMAL)
    cv2.resizeWindow(name, width, height)
    cv2.imshow(name, img)

def callback_thresh(x):
    pass
 
def line_length(x1, y1, x2, y2):
    return math.sqrt((x2 - x1) * (x2 - x1) + (y2 - y1) * (y2 - y1))
    
def img_area(img, area_unit):
    shape = img.shape
    return shape[0] * shape[1] / area_unit
    
def auto_canny(image, sigma=0.33):
	v = np.median(image)
 
	lower = int(max(0, (1.0 - sigma) * v))
	upper = int(min(255, (1.0 + sigma) * v))
	edged = cv2.Canny(image, lower, upper)
 
	return edged
    
def line2line(line1, line2):
    l1x1,l1y1,l1x2,l1y2 = line1[0]
    l2x1,l2y1,l2x2,l2y2 = line2[0]
    l1dx,l1dy = l1x2-l1x1,l1y2-l1y1
    l2dx,l2dy = l2x2-l2x1,l2y2-l2y1
    commonperp_dx,commonperp_dy = (l1dy - l2dy, l2dx-l1dx)

    commonperp_length = math.hypot(commonperp_dx,commonperp_dy)
    if (commonperp_length == 0):
        return 0;
        
    commonperp_normalized_dx = float(commonperp_dx)/float(commonperp_length)
    commonperp_normalized_dy = float(commonperp_dy)/float(commonperp_length)
    
    shortestvector_dx = (l1x1-l2x1)*commonperp_normalized_dx
    shortestvector_dy = (l1y1-l2y1)*commonperp_normalized_dy
    mindist = math.hypot(shortestvector_dx,shortestvector_dy)

    result = mindist
    return result
    
def line2cluster(line, cluster):
    min_dist = line2line(line, cluster[0])
    for l in cluster:
        dist = line2line(line, l)
        if (dist < min_dist):
            min_dist = dist
    
    return min_dist
    
def main():
    eps = 5
    ap = argparse.ArgumentParser()
    ap.add_argument("-s", "--src", required=True,
        help="path to the source file")
    ap.add_argument("-d", "--dst", required=True,
        help="path to the destination file")
    ap.add_argument("-m", "--min", required=True,
        help="min cluster size")
    args = vars(ap.parse_args())
    
    src_path = args["src"]
    dst_path = args["dst"]
    min_cluster_size = args["min"]
    
    img = cv2.imread(src_path)
    original = img
    img = cv2.cvtColor(img,cv2.COLOR_BGR2GRAY)
    entr_img = entropy(img, diamond(5))
    open_window('entropy', entr_img)
    
    hist, bins = np.histogram(img.flatten(), 256, [0,256])
    cdf = hist.cumsum()
    cdf_normalized = cdf * hist.max() / cdf.max()
    cdf_m = np.ma.masked_equal(cdf,0)
    cdf_m = (cdf_m - cdf_m.min())*255/(cdf_m.max()-cdf_m.min())
    cdf = np.ma.filled(cdf_m,0).astype('uint8')
    img = cdf[img]
    img = cv2.bilateralFilter(img, 25,75,75)
    
    np_img = np.asarray(img[:,:])

    thresh_sauvola = threshold_sauvola(np_img, window_size=101)    
    binary_sauvola = np_img > thresh_sauvola
    binary_sauvola = np.logical_not(binary_sauvola)
    binary_sauvola = img_as_ubyte(binary_sauvola)
    kernel = np.ones((5,5),np.uint8)
    binary_sauvola = cv2.erode(binary_sauvola,kernel,iterations = 1)
    binary_sauvola = cv2.dilate(binary_sauvola, kernel, iterations = 1)
    '''
    mll = 200
    lines = cv2.HoughLinesP(binary_sauvola, 1, np.pi/180, 10, minLineLength = mll, maxLineGap = 10)
    
    if lines is None:
        cv2.imwrite(dst_path + '\\0\\' + os.path.split(src_path)[1], original)
        exit(0)
    
    lines = lines.tolist()
    '''
    '''
    line_clusters = []
    lines_length = len(lines)
    for i in range(0, lines_length):
        if lines[i] is None:
            continue
        cluster = []
        cluster.append(lines[i])
        lines[i] = None
        line_clusters.append(cluster)
        for j in range(i, lines_length):
            for k in range(0, len(line_clusters)):
                if lines[j] is None:
                    continue
                if (line2cluster(lines[j], line_clusters[k]) < eps):
                    line_clusters[k].append(lines[j])
                    lines[j] = None
                
    for cluster in line_clusters[:]:
        if len(cluster) < int(min_cluster_size):
            line_clusters.remove(cluster)
        
    
    clusters_count = len(line_clusters)
    print(clusters_count)
    if (clusters_count > 3):
        cv2.imwrite(dst_path + '\\1\\' + os.path.split(src_path)[1], original)
    else:
        cv2.imwrite(dst_path + '\\0\\' + os.path.split(src_path)[1], original)
        
    '''
    cv2.waitKey(0)
    
if __name__ == '__main__':
    main()
