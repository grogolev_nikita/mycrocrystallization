import tkinter as tk
import numpy as np
import os
import cv2
import argparse
import operator

from tkinter import filedialog
from skimage import img_as_ubyte
from skimage.filters import threshold_sauvola

def choose_file():
    root = tk.Tk()
    root.withdraw()

    file_path = filedialog.askopenfilename()
    return file_path

def filter_contours(cnts, area):
    large_cnts = []
    for c in cnts:
        cnt_area = cv2.arcLength(c, True)
        if (cnt_area > area):
            large_cnts.append(c)
            
    return large_cnts
    
def img_area(img, area_unit):
    shape = img.shape
    return shape[0] * shape[1] / area_unit
    
def count_crystals(cnts):
    count = 0
    for c in cnts:
        ( (x, y), (w, h), angle ) = cv2.minAreaRect(c)
        sides_relation = w / h if (w > h) else h / w
        if ( sides_relation >= 3 ):
            count += 1
            
    return count
    
def get_largest_contours(cnts, count):
    cnts_length = []
    for c in cnts:
        cnts_length.append(cv2.arcLength(c, True))
        
    for i in range(len(cnts)):
        for j in range(len(cnts) - 1):
            if (cnts_length[j + 1] < cnts_length[j]):
                (cnts_length[j], cnts_length[j + 1]) = (cnts_length[j + 1], cnts_length[j])
    
    return cnts[len(cnts) - count:]
    
ap = argparse.ArgumentParser()
ap.add_argument("-s", "--src", required=True,
    help="path to the source file")     
ap.add_argument("-d", "--dst", required=True,
    help="path to the destination file")
ap.add_argument("-cs", "--cntsize", required=True,
    help="min contour size")
ap.add_argument("-cc", "--cntcount", required=True,
    help="min contour count")
args = vars(ap.parse_args())

src_path = args["src"]
dst_path = args["dst"]
min_cnt_size = int(args["cntsize"]) * 100
min_cnt_count = int(args["cntcount"])

img = cv2.imread(src_path)
gray = cv2.cvtColor(img,cv2.COLOR_BGR2GRAY)

hist, bins = np.histogram(gray.flatten(), 256, [0,256])
cdf = hist.cumsum()
cdf_normalized = cdf * hist.max() / cdf.max()
cdf_m = np.ma.masked_equal(cdf,0)
cdf_m = (cdf_m - cdf_m.min())*255/(cdf_m.max()-cdf_m.min())
cdf = np.ma.filled(cdf_m,0).astype('uint8')
gray = cdf[gray]
blur = cv2.bilateralFilter(gray,5,175,175)

np_img = np.asarray(blur[:,:])
thresh_sauvola = threshold_sauvola(np_img, window_size=101)    
binary_sauvola = np_img > thresh_sauvola
binary_sauvola = np.logical_not(binary_sauvola)
binary_sauvola = img_as_ubyte(binary_sauvola)

im2, contours, hierarchy = cv2.findContours(binary_sauvola,cv2.RETR_TREE,cv2.CHAIN_APPROX_NONE)
if contours is None:
    cv2.imwrite(dst_path + '\\0\\' + os.path.split(src_path)[1], img)
    exit(0)

contours = filter_contours(contours, min_cnt_size / img_area(img, 2000000))
contours = get_largest_contours(contours, 10)
total_crystals = count_crystals(contours)
print(total_crystals)

if (total_crystals >= min_cnt_count):
    cv2.imwrite(dst_path + '\\1\\' + os.path.split(src_path)[1], img)
else:
    cv2.imwrite(dst_path + '\\0\\' + os.path.split(src_path)[1], img)
    
cv2.waitKey(0)
