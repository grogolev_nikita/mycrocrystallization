import cv2
import numpy as np
import tkinter as tk
import os
import ntpath
import argparse

from tkinter import filedialog
from matplotlib import pyplot as plt

def choose_file():
   root = tk.Tk()
   root.withdraw()

   file_path = filedialog.askopenfilename(initialdir = "E:/Crystalls")
   return file_path

ap = argparse.ArgumentParser()
ap.add_argument("-s", "--src", required=True,
    help="path to the source file")
ap.add_argument("-d", "--dst", required=True,
    help="path to the destination file")
args = vars(ap.parse_args())


src_path = args["src"]
dst_path = args["dst"]
img = cv2.imread(src_path,0)
f = np.fft.fft2(img)
fshift = np.fft.fftshift(f)
magnitude_spectrum = 20*np.log(np.abs(fshift))

cv2.imwrite(dst_path + '//' + os.path.split(src_path)[1], img)
cv2.imwrite(dst_path + '//' + ntpath.basename(src_path).split('.')[0] + '_fourie' + os.path.splitext(src_path)[1], magnitude_spectrum)
