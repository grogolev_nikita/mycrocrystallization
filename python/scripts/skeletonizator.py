import cv2
import numpy as np
import argparse
import os
import ntpath
import tkinter as tk
from skimage import img_as_ubyte
from tkinter import filedialog
from skimage.filters import threshold_sauvola

def choose_file():
   root = tk.Tk()
   root.withdraw()

   file_path = filedialog.askopenfilename()
   return file_path

def remove_outliers(img, window_size):

 
ap = argparse.ArgumentParser()
ap.add_argument("-s", "--src", required=True,
    help="path to the source file")
ap.add_argument("-d", "--dst", required=True,
    help="path to the destination file")
args = vars(ap.parse_args())

src_path = args["src"]
dst_path = args["dst"]

img = cv2.imread(src_path,0)
original = img
size = np.size(img)
skel = np.zeros(img.shape,np.uint8)
img = cv2.medianBlur(img, 9)

np_img = np.asarray(img[:,:])
thresh_sauvola = threshold_sauvola(np_img, window_size=101)    
binary_sauvola = np_img > thresh_sauvola
binary_sauvola = np.logical_not(binary_sauvola)
img = img_as_ubyte(binary_sauvola)
#kernel = np.zeros((3,3),np.uint8)
#img = cv2.dilate(img, kernel)
 
element = cv2.getStructuringElement(cv2.MORPH_CROSS,(3,3))
done = False
 
while( not done):
    eroded = cv2.erode(img,element)
    temp = cv2.dilate(eroded,element)
    temp = cv2.subtract(img,temp)
    skel = cv2.bitwise_or(skel,temp)
    img = eroded.copy()
 
    zeros = size - cv2.countNonZero(img)
    if zeros==size:
        done = True
        
cv2.imwrite(dst_path + '\\' + os.path.split(src_path)[1], original)
cv2.imwrite(dst_path + '\\' + ntpath.basename(src_path).split('.')[0] + '_skel' + os.path.splitext(src_path)[1], skel)