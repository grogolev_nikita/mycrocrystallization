import cv2
import numpy as np
import copy
import argparse
import os
import ntpath
import tkinter as tk

from skimage import img_as_ubyte
from skimage import io
from tkinter import filedialog

def choose_file():
   root = tk.Tk()
   root.withdraw()

   file_path = filedialog.askopenfilename(initialdir = "E:/Crystalls")
   return file_path

def remove_outliers(img, window_size, perimeter_dim=None, step=None):
    if step is None:
        step = int(window_size / 2)
        
    if perimeter_dim is None:
        perimeter_dim = 2
    
    img_copy = copy.deepcopy(img)
    for i in range(0, len(img) - window_size, step):
        for j in range(0, len(img[i]) - window_size, step):
            zero = True
            for k in range(i, i + perimeter_dim):
                for l in range(j, j + window_size):
                    if img[k][l] != 0:
                        zero = False
                        break
                if not zero:
                    break
                
            if not zero:
                continue
                
            for k in range(i + window_size, i + window_size - perimeter_dim, -1):
                for l in range(j, j + window_size):
                    if img[k][l] != 0:
                        zero = False
                        break
                if not zero:
                    break
                
            if not zero:
                continue
                
            for k in range(j, j + perimeter_dim):
                for l in range(i, i + window_size):
                    if img[l][k] != 0:
                        zero = False
                        break
                if not zero:
                    break
            
            if not zero:
                continue
                
            for k in range(j + window_size, j + window_size - perimeter_dim, -1):
                for l in range(i, i + window_size):
                    if img[l][k] != 0:
                        zero = False
                        break
                if not zero:
                    break
                
            if not zero:
                continue
                
            for k in range(i + perimeter_dim, i + window_size - perimeter_dim):
                for l in range(j + perimeter_dim, j + window_size - perimeter_dim):
                    img_copy[k][l] = 0
    
    return img_copy
                
img = io.imread(choose_file(), mode = 'L')
original = img_as_ubyte(img)
img = remove_outliers(img, window_size=30, perimeter_dim=2, step=5)
img = img_as_ubyte(img)
cv2.imwrite('E:\\Crystalls\\filtered_skel.jpg', img)
cv2.imshow('original', original)
cv2.imshow('res', img)
cv2.waitKey(0)

