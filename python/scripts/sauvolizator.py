import argparse
import cv2
from skimage.filters import threshold_sauvola
from skimage import img_as_ubyte

ap = argparse.ArgumentParser()
ap.add_argument("-s", "--src", required=True,
    help="path to the source file")
ap.add_argument("-d", "--dst", required=True,
    help="path to the destination file")
ap.add_argument("-w", "--window", required=True,
    help="size of window")
args = vars(ap.parse_args())

src_path = args["src"]
dst_path = args["dst"]
win_size = int(args["window"])

image = cv2.imread(src_path)
gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
cv2.imwrite('E:\Crystalls\gray208.bmp', gray)

sauvola = gray > threshold_sauvola(gray, win_size)
sauvola = img_as_ubyte(sauvola)
print(dst_path)
cv2.imwrite(dst_path, sauvola)
