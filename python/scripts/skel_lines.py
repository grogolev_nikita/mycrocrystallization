import cv2
import tkinter as tk
import numpy as np
import scipy as sp
import csv
import math
import ntpath
import argparse
import os

from tkinter import filedialog
from skimage import img_as_ubyte
from skimage import io
from skimage.filters import threshold_sauvola

def choose_file():
    root = tk.Tk()
    root.withdraw()

    file_path = filedialog.askopenfilename()
    return file_path
 
def line_length(x1, y1, x2, y2):
    return math.sqrt((x2 - x1) * (x2 - x1) + (y2 - y1) * (y2 - y1))
    
def img_area(img, area_unit):
    shape = img.shape
    return shape[0] * shape[1] / area_unit

def count_long_lines(lines, mld, area):
    long_count = 0
    count = 0
    
    for line in lines:
        for x1, y1, x2, y2 in line:
            dencity = line_length(x1, y1, x2, y2) / area
            if (dencity >= mld):
                long_count += 1
            if (dencity >= mld / 2.0):
                count += 1
                            
    return (count, long_count)

def main():
    ap = argparse.ArgumentParser()
    ap.add_argument("-s", "--src", required=True,
        help="path to the source file")
    ap.add_argument("-d", "--dst", required=True,
        help="path to the destination file")
    ap.add_argument("-mld", required=True,
        help="min line dencity")

    args = vars(ap.parse_args())
    
    src_path = args["src"]
    dst_path = args["dst"]
    mll = 80
    mlg = 20
    mld = float(args["mld"])
    
    # read image
    img = cv2.imread(src_path, 0)
    canny = cv2.Canny(img, 100, 200)
    
    # detect lines
    lines = cv2.HoughLinesP(canny, 1, np.pi/180, 10, minLineLength = mll, maxLineGap = mlg)
    if lines is None:
        exit(0)
    
    area = img_area(img, 1000)
    for line in lines:
        for x1, y1, x2, y2 in line:
            llen = line_length(x1, y1, x2, y2)
            print(llen, llen / area, area)
    
    (lines_count, long_lines_count) = count_long_lines(lines, mld, area)
    
    if (long_lines_count > 1):
        cv2.imwrite(dst_path + '//1//' + os.path.split(src_path)[1], canny)
    else:
        long_lines_count = count_long_lines(lines, mld / 2.0, area)
        if (lines_count > 5):
            cv2.imwrite(dst_path + '//1//' + os.path.split(src_path)[1], canny)
        else:
            cv2.imwrite(dst_path + '//0//' + os.path.split(src_path)[1], canny)
    
if __name__ == '__main__':
    main()