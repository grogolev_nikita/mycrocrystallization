import cv2
import tkinter as tk
import numpy as np
import scipy as sp
import math
import argparse
import os
import ntpath
from skimage.filters.rank import entropy

from tkinter import filedialog
from skimage import img_as_ubyte
from skimage import io
from skimage.filters import (threshold_otsu, threshold_niblack,
                             threshold_sauvola)
from skimage.filters.rank import entropy
from skimage.morphology import diamond

def choose_file():
    root = tk.Tk()
    root.withdraw()

    file_path = filedialog.askopenfilename()
    return file_path

def open_window(name, img, width=800, height=600):
    cv2.namedWindow(name, cv2.WINDOW_NORMAL)
    cv2.resizeWindow(name, width, height)
    cv2.imshow(name, img)
 
def line_length(x1, y1, x2, y2):
    return math.sqrt((x2 - x1) * (x2 - x1) + (y2 - y1) * (y2 - y1))
    
def img_area(img, area_unit):
    shape = img.shape
    return shape[0] * shape[1] / area_unit
    
def line2line(line1, line2):
    l1x1,l1y1,l1x2,l1y2 = line1[0]
    l2x1,l2y1,l2x2,l2y2 = line2[0]
    l1dx,l1dy = l1x2-l1x1,l1y2-l1y1
    l2dx,l2dy = l2x2-l2x1,l2y2-l2y1
    commonperp_dx,commonperp_dy = (l1dy - l2dy, l2dx-l1dx)

    commonperp_length = math.hypot(commonperp_dx,commonperp_dy)
    if (commonperp_length == 0):
        return 0;
        
    commonperp_normalized_dx = float(commonperp_dx)/float(commonperp_length)
    commonperp_normalized_dy = float(commonperp_dy)/float(commonperp_length)
    
    shortestvector_dx = (l1x1-l2x1)*commonperp_normalized_dx
    shortestvector_dy = (l1y1-l2y1)*commonperp_normalized_dy
    mindist = math.hypot(shortestvector_dx,shortestvector_dy)

    result = mindist
    return result
    
def line2cluster(line, cluster):
    min_dist = line2line(line, cluster[0])
    for l in cluster:
        dist = line2line(line, l)
        if (dist < min_dist):
            min_dist = dist
    
    return min_dist
    
def draw_lines(img, lines):
    for line in lines:
        for x1, y1, x2, y2 in line:
            cv2.line(img, (x1, y1), (x2, y2), (255, 0, 0))
    
def main():
    ap = argparse.ArgumentParser()
    ap.add_argument("-s", "--src", required=True,
        help="path to the source file")
    ap.add_argument("-d", "--dst", required=True,
        help="path to the destination file")
    ap.add_argument("-l", "--mll", required=True,
        help="min line length")
    ap.add_argument("-g", "--mlg", required=True,
        help="max line gap")
    args = vars(ap.parse_args())
    
    src_path = args["src"]
    dst_path = args["dst"]
    min_line_length = int(args["mll"])
    max_line_gap = int(args["mlg"])
    
    img = cv2.imread(src_path)
    img = cv2.cvtColor(img,cv2.COLOR_BGR2GRAY)
    original = img
    black_img = np.zeros(img.shape,np.uint8)
    
    hist, bins = np.histogram(img.flatten(), 256, [0,256])
    cdf = hist.cumsum()
    cdf_normalized = cdf * hist.max() / cdf.max()
    cdf_m = np.ma.masked_equal(cdf,0)
    cdf_m = (cdf_m - cdf_m.min())*255/(cdf_m.max()-cdf_m.min())
    cdf = np.ma.filled(cdf_m,0).astype('uint8')
    img = cdf[img]
    img = cv2.bilateralFilter(img, 25,75,75)
    
    np_img = np.asarray(img[:,:])
    thresh_sauvola = threshold_sauvola(np_img, window_size=101)    
    binary_sauvola = np_img > thresh_sauvola
    binary_sauvola = np.logical_not(binary_sauvola)
    binary_sauvola = img_as_ubyte(binary_sauvola)
    lines = cv2.HoughLinesP(binary_sauvola, 1, np.pi/180, 10, minLineLength = min_line_length, maxLineGap = max_line_gap)
    if lines is not None:
        print(len(lines))
        draw_lines(black_img, lines)
        
    cv2.imwrite(dst_path + '\\' + os.path.split(src_path)[1], original)
    cv2.imwrite(dst_path + '\\' + ntpath.basename(src_path).split('.')[0] + '_lines' + os.path.splitext(src_path)[1], black_img)

    
if __name__ == '__main__':
    main()
