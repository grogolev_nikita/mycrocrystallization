import cv2
import tkinter as tk
import numpy as np
import scipy as sp
import csv
import math
import argparse
import os

from tkinter import filedialog
from skimage import img_as_ubyte
from skimage import io
from skimage.filters import threshold_sauvola

def choose_file():
    root = tk.Tk()
    root.withdraw()

    file_path = filedialog.askopenfilename()
    return file_path
 
def line_length(x1, y1, x2, y2):
    return math.sqrt((x2 - x1) * (x2 - x1) + (y2 - y1) * (y2 - y1))

def line_length(line):
    return line_length(line[0][0], line[0][1], line[1][0], line[1][1])
    
def img_area(img, area_unit):
    shape = img.shape
    return shape[0] * shape[1] / area_unit
    
def main():
    ap = argparse.ArgumentParser()
    ap.add_argument("-s", "--src", required=True,
        help="path to the source file")
    ap.add_argument("-mll", required=True,
        help="min line length")
    ap.add_argument("-mlg", required=True,
        help="max line gap")
    args = vars(ap.parse_args())
    
    src_path = args["src"]
    mll = args["mll"]
    mlg = args["mlg"]
    
    # read image
    img = cv2.imread(src_path, 0)
    
    # calculate hist
    hist, bins = np.histogram(img.flatten(), 256, [0,256])
    cdf = hist.cumsum()
    cdf_normalized = cdf * hist.max() / cdf.max()
    cdf_m = np.ma.masked_equal(cdf,0)
    cdf_m = (cdf_m - cdf_m.min())*255/(cdf_m.max()-cdf_m.min())
    cdf = np.ma.filled(cdf_m,0).astype('uint8')
    img = cdf[img]
    img = cv2.bilateralFilter(img, 25,75,75)
    
    # sauvola threshold
    np_img = np.asarray(img[:,:])
    thresh_sauvola = threshold_sauvola(np_img, window_size=101)    
    binary_sauvola = np_img > thresh_sauvola
    binary_sauvola = np.logical_not(binary_sauvola)
    binary_sauvola = img_as_ubyte(binary_sauvola)
    kernel = np.ones((5,5),np.uint8)
    binary_sauvola = cv2.erode(binary_sauvola,kernel,iterations = 1)
    binary_sauvola = cv2.dilate(binary_sauvola, kernel, iterations = 1)
    
    # detect lines
    lines = cv2.HoughLinesP(binary_sauvola, 1, np.pi/180, 10, minLineLength = mll, maxLineGap = mlg)
    
    # calculate lines hist
    lines_hist = []
    header = range(mll, mll * 10, mll)
    for i in header:
        lines_hist.append((i, 0))
    
    lines_hist = dict(lines_hist)
    
    if lines is not None:
        for line in lines:
            line_len = line_length(x1, y1, x2, y2)
            for key in lines_hist:
                if line_len <= key:
                    lines_hist[key] += 1
    
    fields = list(lines_hist.values())
    fields.append(int(os.path.splitext(src_path)[0]))
     
    with open(r'lines_hist.csv', 'a') as f:
        writer = csv.writer(f)
        writer.writerow(fields)
        
if __name__ == '__main__':
    main()