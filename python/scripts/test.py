import cv2
import numpy as np
import argparse
import os
import ntpath
import tkinter as tk
from skimage import img_as_ubyte
from tkinter import filedialog
from skimage.filters import threshold_sauvola

def choose_file():
   root = tk.Tk()
   root.withdraw()

   file_path = filedialog.askopenfilename()
   return file_path
def open_window(name, img, width=800, height=600):
    cv2.namedWindow(name, cv2.WINDOW_NORMAL)
    cv2.resizeWindow(name, width, height)
    cv2.moveWindow(name, 0, 0)
    cv2.imshow(name, img)

def filter_contours(cnts, area):
    large_cnts = []
    for c in cnts:
        cnt_area = cv2.arcLength(c, True)
        if (cnt_area > area):
            large_cnts.append(c)
            
    return large_cnts

def draw_fit_lines(img, cnts):
    rows,cols = img.shape[:2]
    for c in cnts:
        max_x = 0
        min_x = cols - 1
        for p in c:
            if p[0][0] > max_x:
                max_x = p[0][0]
            if p[0][0] < min_x:
                min_x = p[0][0] 
        
        [vx,vy,x,y] = cv2.fitLine(c, cv2.DIST_L2,0,0.01,0.01)
        lefty = int((-x*vy/vx) + y)
        righty = int(((cols-x)*vy/vx)+y)
        
        m = (righty - lefty) / (cols - 1)
        lefty = int(m * min_x + lefty)
        righty = int(m * (max_x - (cols - 1)) + righty)
        cv2.line(img,(max_x,righty),(min_x,lefty),(0,255,0),2)
        
            
img = cv2.imread(choose_file())
original = img
img = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
size = np.size(img)
skel = np.zeros(img.shape,np.uint8)
img = cv2.medianBlur(img, 9)

np_img = np.asarray(img[:,:])
thresh_sauvola = threshold_sauvola(np_img, window_size=101)    
binary_sauvola = np_img > thresh_sauvola
not_binary_sauvola = np.logical_not(binary_sauvola)
not_sauvola = img_as_ubyte(not_binary_sauvola)
sauvola = img_as_ubyte(binary_sauvola)
im2, contours, hierarchy = cv2.findContours(not_sauvola,cv2.RETR_TREE,cv2.CHAIN_APPROX_NONE)
contours = filter_contours(contours, 1000)
cv2.drawContours(original, contours, -1, (255,0,0), 2)
draw_fit_lines(original, contours)

open_window('contours', original)
cv2.waitKey(0)
#cv2.imwrite(dst_path + '\\' + os.path.split(src_path)[1], original)
#cv2.imwrite(dst_path + '\\' + ntpath.basename(src_path).split('.')[0] + '_skel' + os.path.splitext(src_path)[1], skel)