#include "Sauvola.h"

double calc_local_stats(p_mat im, p_mat map_m, p_mat map_s, int winx, int winy)
{
	p_mat im_sum = create_empty_mat();
	p_mat im_sum_sq = create_empty_mat();
	im_integral(im, im_sum, im_sum_sq, CV_64F);

	double m, s, max_s, sum, sum_sq;
	int wxh = winx / 2;
	int wyh = winy / 2;
	int x_firstth = wxh;
	int y_lastth = get_rows(im) - wyh - 1;
	int y_firstth = wyh;
	double winarea = winx*winy;

	max_s = 0;
	for (int j = y_firstth; j <= y_lastth; j++) {
		sum = sum_sq = 0;

		sum = get_double(im_sum, j - wyh + winy, winx) - get_double(im_sum, j - wyh, winx) - get_double(im_sum, j - wyh + winy, 0) + get_double(im_sum, j - wyh, 0);
		sum_sq = get_double(im_sum_sq, j - wyh + winy, winx) - get_double(im_sum_sq, j - wyh, winx) - get_double(im_sum_sq, j - wyh + winy, 0) + get_double(im_sum_sq, j - wyh, 0);

		m = sum / winarea;
		s = c_sqrt((sum_sq - m*sum) / winarea);
		if (s > max_s) max_s = s;

		set_float(map_m, x_firstth, j, m);
		set_float(map_s, x_firstth, j, s);

		// Shift the window, add and remove	new/old values to the histogram
		for (int i = 1; i <= get_cols(im) - winx; i++) {

			// Remove the left old column and add the right new column
			sum -= get_double(im_sum, j - wyh + winy, i) - get_double(im_sum, j - wyh, i) - get_double(im_sum, j - wyh + winy, i - 1) + get_double(im_sum, j - wyh, i - 1);
			sum += get_double(im_sum, j - wyh + winy, i + winx) - get_double(im_sum, j - wyh, i + winx) - get_double(im_sum, j - wyh + winy, i + winx - 1) + get_double(im_sum, j - wyh, i + winx - 1);

			sum_sq -= get_double(im_sum_sq, j - wyh + winy, i) - get_double(im_sum_sq, j - wyh, i) - get_double(im_sum_sq, j - wyh + winy, i - 1) + get_double(im_sum_sq, j - wyh, i - 1);
			sum_sq += get_double(im_sum_sq, j - wyh + winy, i + winx) - get_double(im_sum_sq, j - wyh, i + winx) - get_double(im_sum_sq, j - wyh + winy, i + winx - 1) + get_double(im_sum_sq, j - wyh, i + winx - 1);

			m = sum / winarea;
			s = c_sqrt((sum_sq - m*sum) / winarea);
			if (s > max_s) max_s = s;

			set_float(map_m, i + wxh, j, m);
			set_float(map_s, i + wxh, j, s);
		}
	}
	return max_s;
}

void sauvola(p_mat im, p_mat output, int winx, int winy, double k, double dR)
{
	double m, s, max_s;
	double th = 0;
	double min_I, max_I;
	int wxh = winx / 2;
	int wyh = winy / 2;
	int x_firstth = wxh;
	int x_lastth = get_cols(im) - wxh - 1;
	int y_lastth = get_rows(im) - wyh - 1;
	int y_firstth = wyh;

	// Create local statistics and store them in a double matrices
	p_mat map_m = create_zero_mat(get_rows(im), get_cols(im), CV_32F);
	p_mat map_s = create_zero_mat(get_rows(im), get_cols(im), CV_32F);
	max_s = calc_local_stats(im, map_m, map_s, winx, winy);

	min_max_loc(im, &min_I, &max_I);

	p_mat thsurf = create_mat(get_rows(im), get_cols(im), CV_32F);

	// Create the threshold surface, including border processing
	// ----------------------------------------------------

	for (int j = y_firstth; j <= y_lastth; j++) {

		// NORMAL, NON-BORDER AREA IN THE MIDDLE OF THE WINDOW:
		for (int i = 0; i <= get_cols(im) - winx; i++) {
			m = get_float(map_m, i + wxh, j);
			s = get_float(map_s, i + wxh, j);

			th = m * (1 + k*(s / dR - 1));

			set_float(thsurf, i + wxh, j, th);

			if (i == 0) {
				// LEFT BORDER
				for (int i = 0; i <= x_firstth; ++i)
					set_float(thsurf, i, j, th);

				// LEFT-UPPER CORNER
				if (j == y_firstth)
					for (int u = 0; u < y_firstth; ++u)
						for (int i = 0; i <= x_firstth; ++i)
							set_float(thsurf, i, u, th);

				// LEFT-LOWER CORNER
				if (j == y_lastth)
					for (int u = y_lastth + 1; u < get_rows(im); ++u)
						for (int i = 0; i <= x_firstth; ++i)
							set_float(thsurf, i, u, th);
			}

			// UPPER BORDER
			if (j == y_firstth)
				for (int u = 0; u < y_firstth; ++u)
					set_float(thsurf, i + wxh, u, th);

			// LOWER BORDER
			if (j == y_lastth)
				for (int u = y_lastth + 1; u < get_rows(im); ++u)
					set_float(thsurf, i + wxh, u, th);
		}

		// RIGHT BORDER
		for (int i = x_lastth; i < get_cols(im); ++i)
			set_float(thsurf, i, j, th);

		// RIGHT-UPPER CORNER
		if (j == y_firstth)
			for (int u = 0; u < y_firstth; ++u)
				for (int i = x_lastth; i < get_cols(im); ++i)
					set_float(thsurf, i, u, th);

		// RIGHT-LOWER CORNER
		if (j == y_lastth)
			for (int u = y_lastth + 1; u < get_rows(im); ++u)
				for (int i = x_lastth; i < get_cols(im); ++i)
					set_float(thsurf, i, u, th);
	}

	for (int x = 0; x < get_rows(im); x++)
	{
		for (int y = 0; y < get_cols(im); y++)
		{
			if (get_float(im, x, y) >= get_float(thsurf, x, y))
			{
				set_float(output, x, y, 255);
			}
			else
			{
				set_float(output, x, y, 0);
			}
		}
	}
}