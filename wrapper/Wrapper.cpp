#include "Wrapper.h"
#include <opencv2\core\core.hpp>
#include <cmath>

/**
* Native C wrapper implementation.
* Actually is cpp file
*/

p_mat create_empty_mat()
{
	Mat* mat = new Mat();
	return mat;
}

p_mat create_mat(int height, int width, int type)
{
	Mat* mat = new Mat(height, width, type);
	return mat;
}

int get_rows(p_mat mat)
{
	Mat* real_mat = static_cast<Mat*>(mat);
	return real_mat->rows;
}

int get_cols(p_mat mat)
{
	Mat* real_mat = static_cast<Mat*>(mat);
	return real_mat->cols;
}

void set_byte(p_mat mat, int y, int x, byte value)
{
	Mat* real_mat = static_cast<Mat*>(mat);
	real_mat->at<unsigned char>(y, x) = value;
}

byte get_byte(p_mat mat, int y, int x)
{
	Mat* real_mat = static_cast<Mat*>(mat);
	return real_mat->at<unsigned char>(y, x);
}

float get_float(p_mat mat, int y, int x)
{
	Mat* real_mat = static_cast<Mat*>(mat);
	return real_mat->at<float>(y, x);
}

void set_float(p_mat mat, int y, int x, float value)
{
	Mat* real_mat = static_cast<Mat*>(mat);
	real_mat->at<float>(y, x) = value;
}

float get_float(p_mat mat, int y, int x)
{
	Mat* real_mat = static_cast<Mat*>(mat);
	return real_mat->at<float>(y, x);
}

void set_float(p_mat mat, int y, int x, float value)
{
	Mat* real_mat = static_cast<Mat*>(mat);
	real_mat->at<float>(y, x) = value;
}

double get_double(p_mat mat, int y, int x)
{
	Mat* real_mat = static_cast<Mat*>(mat);
	return real_mat->at<double>(y, x);
}

void set_double(p_mat mat, int y, int x, double value)
{
	Mat* real_mat = static_cast<Mat*>(mat);
	real_mat->at<double>(y, x) = value;
}

p_mat create_zero_mat(int height, int width, int type)
{
	return cv::zeros(height, width, type);
}

void im_integral(p_mat im, p_mat im_sum, p_mat im_sum_sq, int type)
{
	cv::integral(im, im_sum, im_sum_sq, type);
}

void min_max_loc(p_mat im, int& min, int& max)
{
	cv::minMaxLoc(im, min, max);
}

void free_mat(p_mat mat)
{
	if (mat != std::nullptr)
		delete mat;
}

float c_sqrt(float x)
{
	return sqrt(x);
}
