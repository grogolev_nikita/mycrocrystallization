#define CV_64F 6
#define CV_32F 5

#ifdef __cplusplus__
extern "C" {
#endif
 
typedef void* p_mat;
typedef unsigned char byte;

double get_double(p_mat mat, int x, int y);
void set_double(p_mat mat, int x, int y, double value);
byte get_byte(p_mat mat, int x, int y);
void set_byte(p_mat mat, int x, int y, byte value);
float get_float(p_mat mat, int x, int y);
void set_float(p_mat mat, int x, int y, float value);
void im_integral(p_mat im, p_mat im_sum, p_mat im_sum_sq, int type);
double calc_local_stats(p_mat im, p_mat map_m, p_mat map_s, int winx, int winy);
void sauvola(p_mat im, p_mat output, int winx, int winy, double k, double dR);
int get_rows(p_mat mat);
int get_cols(p_mat mat);   
float c_sqrt(float x);
   
p_mat create_empty_mat();
p_mat create_mat(int height, int width, int type);
p_mat create_zero_mat(int height, int width, int type); 
void free_mat(p_mat mat);
 
#ifdef __cplusplus__
};
#endif
 