#define _CRT_SECURE_NO_WARNINGS

#include "Structs.h"
#include <iostream>
#include <fstream>
#include <iomanip>

using namespace cv;

double rad(double alpha);
void printBandInfo(BandInfo &bi, std::ostream &os);
void printHelp();
char* getCmdOption(char ** begin, char ** end, const std::string & option);
bool cmdOptionExists(char** begin, char** end, const std::string& option);
void upPass(void*);  // TODO: split image processing to two threads
void downPass(void*);

int main(int argc, char* argv[])
{
	if (argc < 3)
	{
		printHelp();
		return -1;
	}

	const char* src = getCmdOption(&argv[0], &argv[argc], "-src");
	const char* dst = getCmdOption(&argv[0], &argv[argc], "-dst");
	const char* csvFileName = getCmdOption(&argv[0], &argv[argc], "-outcsv");
	const char* sThresh = getCmdOption(&argv[0], &argv[argc], "-thresh");
	const char* sStep = getCmdOption(&argv[0], &argv[argc], "-step");
	const char* sRStep = getCmdOption(&argv[0], &argv[argc], "-rstep");
	const char* sMinBandDencity = getCmdOption(&argv[0], &argv[argc], "-drawBands");
	const int step = sStep ? atoi(sStep) : DEFAULT_STEP;
	const int rstep = sRStep ? atoi(sRStep) : DEFAULT_RSTEP;
	const int thresh = sThresh ? atoi(sThresh) : DEFAULT_THRESH;
	const double minBandDencity = sMinBandDencity ? atof(sMinBandDencity) : 0;
	bool debug = cmdOptionExists(&argv[0], &argv[argc], "-debug");
	bool drawBands = sMinBandDencity;

	if (!csvFileName)
		return -1;

	std::ofstream ofs(csvFileName);
	ofs.imbue(std::locale(std::locale::classic(), new Comma));
	ofs << "number;angle;x1;y1;x2;y2;width;area;white;whiteOnArea" << std::endl;

	const int minAngle = 0;
	const int straightAngle = 90;
	const int maxAngle = 360;
	const int upperInitialAngle = straightAngle - 1;
	const int bottomInitialAngle = minAngle + 1;
	int bandCounter = 0;

	Mat img = imread(src);
	Mat original;
	img.copyTo(original);
	cvtColor(img, img, CV_BGR2GRAY);

	Line upperInitialLine(-tan(rad(upperInitialAngle)), 0.0);
	Line bottomInitialLine(tan(rad(bottomInitialAngle)), img.rows);

	std::vector<Line> lines;
	for (int i = 0; i < straightAngle; i += rstep)
	{
		int currentAngle = straightAngle + i + 1;

		double tg = tan(-rad(upperInitialAngle - i));
		double b = step * sqrt(tg * tg + 1);

		for (int j = 1; ; j++)
		{
			Line nextLine = upperInitialLine.moveOY(j*b);
			lines.push_back(nextLine);
			if (nextLine.y(img.cols) > img.rows)
				break;
		}

		for (int j = 0; j < lines.size() - 1; j++)
		{
			Line upper = lines[j];
			Line bottom = lines[j + 1];

			int upperY = (upper.x(0) > img.cols) ? upper.y(img.cols) : 0;
			int bottomY = (upper.x(img.rows) < 0) ? bottom.y(0) : img.rows;

			BandInfo bandInfo;
			memset(&bandInfo, 0, sizeof(BandInfo));

			upperY = (upperY < 0) ? 0 : upperY;
			bottomY = (bottomY > img.rows) ? img.rows : bottomY;
			bandInfo.angle = currentAngle;
			bandInfo.y1 = bottomY;
			bandInfo.y2 = upperY;
			bandInfo.x1 = bottom.x(bottomY);
			bandInfo.x2 = upper.x(upperY);
			bandInfo.width = step;
			bandInfo.number = ++bandCounter;

			for (int y = upperY; y < bottomY; y++)
			{
				int leftX = upper.x(y);
				int rightX = bottom.x(y);
				leftX = (leftX < 0) ? 0 : leftX;
				rightX = (rightX > img.cols) ? img.cols : rightX;

				for (int x = leftX; x < rightX; x++)
				{
					if (img.at<uchar>(y, x) == 255)
						bandInfo.whitePxDencity++;
				}

				bandInfo.area += rightX - leftX;
			}

			bandInfo.dencityAreaRatio = (double)bandInfo.whitePxDencity * 100 / bandInfo.area;
			if (bandInfo.whitePxDencity > thresh)
				printBandInfo(bandInfo, ofs);

			if (drawBands && (bandInfo.dencityAreaRatio > minBandDencity) && (bandInfo.whitePxDencity > thresh))
			{
				Vec4i first = upper.toCvLine(original.rows, original.cols);
				Vec4i second = bottom.toCvLine(original.rows, original.cols);
				cv::line(original, Point(first[0], first[1]), Point(first[2], first[3]), RED, 1);
				cv::line(original, Point(second[0], second[1]), Point(second[2], second[3]), RED, 1);
			}
		}

		lines.clear();
		upperInitialLine.rotate(rstep);
	}

	for (int i = 0; i < straightAngle; i += rstep)
	{
		int currentAngle = straightAngle + straightAngle + i + 1;

		double tg = tan(rad(bottomInitialAngle - i));
		double b = step * sqrt(tg * tg + 1);

		for (int j = 1; ; j++)
		{
			Line nextLine = bottomInitialLine.moveOY(-j*b);
			lines.push_back(nextLine);
			if (nextLine.y(img.cols) < 0)
				break;
		}

		for (int j = 0; j < lines.size() - 1; j++)
		{
			Line upper = lines[j + 1];
			Line bottom = lines[j];

			int upperY = (upper.x(0) < 0) ? upper.y(0) : 0;
			int bottomY = (upper.x(img.rows) > img.cols) ? bottom.y(img.cols) : img.rows;

			BandInfo bandInfo;
			memset(&bandInfo, 0, sizeof(BandInfo));

			upperY = (upperY < 0) ? 0 : upperY;
			bottomY = (bottomY > img.rows) ? img.rows : bottomY;
			bandInfo.angle = currentAngle;
			bandInfo.y1 = bottomY;
			bandInfo.y2 = upperY;
			bandInfo.x1 = bottom.x(bottomY);
			bandInfo.x2 = upper.x(upperY);
			bandInfo.width = step;
			bandInfo.number = ++bandCounter;

			for (int y = upperY; y < bottomY; y++)
			{
				int leftX = bottom.x(y);
				int rightX = upper.x(y);
				leftX = (leftX < 0) ? 0 : leftX;
				rightX = (rightX > img.cols) ? img.cols : rightX;

				for (int x = leftX; x < rightX; x++)
				{
					if (img.at<uchar>(y, x) == 255)
						bandInfo.whitePxDencity++;
				}

				bandInfo.area += rightX - leftX;
			}
			bandInfo.dencityAreaRatio = (double)bandInfo.whitePxDencity * 100 / bandInfo.area;

			if (drawBands && (bandInfo.dencityAreaRatio > minBandDencity) && (bandInfo.whitePxDencity > thresh))
			{
				Vec4i first = upper.toCvLine(original.rows, original.cols);
				Vec4i second = bottom.toCvLine(original.rows, original.cols);
				cv::line(original, Point(first[0], first[1]), Point(first[2], first[3]), RED, 1);
				cv::line(original, Point(second[0], second[1]), Point(second[2], second[3]), RED, 1);
			}

			if (bandInfo.whitePxDencity > thresh)
				printBandInfo(bandInfo, ofs);
		}

		lines.clear();
		bottomInitialLine.rotate(rstep);
	}

	if (debug)
	{
		for (int i = 0; i < img.rows; i += 50)
		{
			std::string buf = std::to_string(i);
			putText(original, buf, Point(5, i),
				FONT_HERSHEY_COMPLEX_SMALL, 0.5, BLUE, 1, CV_AA);
		}

		for (int i = 0; i < img.cols; i += 50)
		{
			std::string buf = std::to_string(i);
			putText(original, buf, Point(i, 15),
				FONT_HERSHEY_COMPLEX_SMALL, 0.5, BLUE, 1, CV_AA);
		}
	}

	if (dst)
		imwrite(dst, original);

	ofs.close();
}

bool cmdOptionExists(char* *begin, char* *end, const std::string& option)
{
	return std::find(begin, end, option) != end;
}

char* getCmdOption(char* *begin, char* *end, const std::string & option)
{
	char* *itr = std::find(begin, end, option);
	if (itr != end && ++itr != end)
	{
		return *itr;
	}

	return 0;
}

void printBandInfo(BandInfo &bi, std::ostream &os)
{
	os << bi.number << ';' << bi.angle << ';' << bi.x1 <<
		';' << bi.y1 << ';' << bi.x2 << ';' << bi.y2 <<
		';' << bi.width << ';' << bi.area << ';' << bi.whitePxDencity <<
		';' << std::setprecision(4) << bi.dencityAreaRatio << std::endl;
}

double rad(double alpha)
{
	return alpha * PI / 180.0;
}

void printHelp()
{
	std::cout << std::endl
		<< "-src - image path" << std::endl
		<< "-outcsv - csv file path" << std::endl
		<< "-thresh (optional) - threshold for csv file rows" << std::endl
		<< "-step (optional) - line shift step" << std::endl
		<< "-rstep (optional) - line rotation step" << std::endl
		<< "-debug (optional) - draw axis values" << std::endl
		<< "-drawBands (optional) - draw bands with specified white pixels dencity" << std::endl
		<< "-dst (optional) - output image path" << std::endl;
}