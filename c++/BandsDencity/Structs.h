#pragma once
#include "Consts.h"

struct LocalMat
{
	Mat localMat;
	std::ostream ostream;
};
 
struct Comma final : std::numpunct<char>
{
	char do_decimal_point() const override { return ','; }
};

struct BandInfo
{
	int angle;
	int x1, y1;
	int x2, y2;
	int width;
	int whitePxDencity;
	int area;
	int number;
	double dencityAreaRatio;
};

struct Line
{
	Line(double a, double b)
	{
		this->a = a;
		this->b = b;
	}

	double a, b;

	double x(double y)
	{
		if (a == 0.0)
			return nan("");

		return (y - b) / a;
	}

	double y(double x)
	{
		return a * x + b;
	}

	void rotate(double alpha)
	{
		double currentAngle = atan(a);
		a = tan(rad(currentAngle * 180.0 / PI + alpha));
	}

	Line moveOY(int offset)
	{
		Line line(a, b + offset);
		return line;
	}

	Vec4i toCvLine(int rows, int cols)
	{
		Vec4i cvLine;

		cvLine[0] = x(0);
		cvLine[1] = 0;
		cvLine[2] = x(rows);
		cvLine[3] = rows;
		return cvLine;
	}
};