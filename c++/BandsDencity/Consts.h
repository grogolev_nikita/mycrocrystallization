#pragma once
#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc.hpp>

#define RED CV_RGB(255, 0, 0)
#define BLUE CV_RGB(0, 0, 255)
#define DEFAULT_STEP 20
#define DEFAULT_RSTEP 5
#define DEFAULT_THRESH -1
#define PI 3.14159265358979323846