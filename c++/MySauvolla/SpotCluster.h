#pragma once
#include "cluster.h"
class SpotCluster :
	public Cluster
{
	int m_cxRadius;
	int m_spotLinesCount;
	CvSeq* m_spotLines;
	std::pair<int, int> m_coords;

	size_t GetLineLength(int x1, int x2, int y1, int y2)
	{
		return sqrt((x1 - x2)*(x1 - x2) + (y1 - y2)*(y1 - y2));
	}

	 
public:
	SpotCluster(CvSeq* lines, int count);
	~SpotCluster(void);
	float getAverageX();
	float getAverageY();
	float getAverageRadius();
	int getMaxDiametr();
};

