#pragma once
#include <fstream>
#include <Windows.h>

class CSVDataExporter
{
private:
	std::ofstream m_csvFile;

public:
	CSVDataExporter(const char* fileName, char* headers[], int headersCount);
	~CSVDataExporter();
	
	template<typename T>
	void addFieldsToRow(T* params, int count)
	{
		for (int i = 0; i < count; i++)
		{
			m_csvFile << params[i] << ';';
		}
	}
	
	void closeRow()
	{
		m_csvFile << std::endl;
	}
};

