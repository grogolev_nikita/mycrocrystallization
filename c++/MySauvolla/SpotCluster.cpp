#include "stdafx.h"
#include "SpotCluster.h"


SpotCluster::SpotCluster(CvSeq* lines, int count) : m_spotLinesCount(count)
{
	CvMemStorage *memStorage = cvCreateMemStorage(0);
	m_spotLines = cvCreateSeq(0, sizeof(CvSeq), sizeof(CvPoint)*2, memStorage);
	for (int i = 0; i < m_spotLinesCount; i++)
	{
		CvPoint* points( (CvPoint*) cvGetSeqElem(lines, i) );
		cvSeqPush( m_spotLines, points);
	}
}


SpotCluster::~SpotCluster(void)
{
}

float SpotCluster::getAverageRadius()
{
	float avRad = 0;
	int counter = 0;
	for (int i = 0; i < m_spotLinesCount; i++)
	{
		CvPoint* points = (CvPoint*) cvGetSeqElem(m_spotLines, i);
		avRad += GetLineLength(points[0].x, points[1].x, points[0].y, points[1].y);
		counter++;
	}

	return avRad / counter;
}

float SpotCluster::getAverageX()
{
	CvPoint* firstPoint = (CvPoint*) cvGetSeqElem(m_spotLines, 0);

	int minX = (firstPoint[0].x + firstPoint[1].x) / 2;
	int maxX = (firstPoint[0].x + firstPoint[1].x) / 2;
	for (int i = 1; i < m_spotLinesCount; i++)
	{
		CvPoint* points = (CvPoint*) cvGetSeqElem(m_spotLines, i);
		float avX = (points[0].x + points[1].x) / 2;

		if ( avX < minX )
			minX = avX;

		if ( avX > maxX )
			maxX = avX;
	}

	return (minX + maxX) / 2;
}

float SpotCluster::getAverageY()
{
	CvPoint* firstPoint = (CvPoint*) cvGetSeqElem(m_spotLines, 0);

	int minY = (firstPoint[0].y + firstPoint[1].y) / 2;
	int maxY = (firstPoint[0].y + firstPoint[1].y) / 2;
	for (int i = 1; i < m_spotLinesCount; i++)
	{
		CvPoint* points = (CvPoint*) cvGetSeqElem(m_spotLines, i);
		float avY = (points[0].y + points[1].y) / 2;

		if ( avY < minY )
			minY = avY;

		if ( avY > maxY )
			maxY = avY;
	}

	return (minY + maxY) / 2;
}

int SpotCluster::getMaxDiametr()
{
	int maxDiam = 0;
	for (int i = 0; i < m_spotLinesCount; i++)
	{
		CvPoint* points = (CvPoint*) cvGetSeqElem(m_spotLines, i);
		int length = GetLineLength(points[0].x, points[1].x, points[0].y, points[1].y);
		if ( length > maxDiam )
			maxDiam = length;
	}

	return maxDiam;
}