#pragma once

#define TOTAL		{360, 701, 651, 779, 867}
#define AVERAGE		{134, 142, 186, 213, 265}
#define MAX			{308, 455, 1002, 1171, 1526}

#define TOTAL_ZERO		360
#define TOTAL_ONE		701
#define TOTAL_TWO		651
#define TOTAL_THREE		779
#define TOTAL_FOUR		867
#define TOTAL_FIVE		813

#define AVERAGE_ZERO	134
#define AVERAGE_ONE		142
#define AVERAGE_TWO		186
#define AVERAGE_THREE	213
#define AVERAGE_FOUR	265
#define AVERAGE_FIVE	193

#define MAX_ZERO		308
#define MAX_ONE			455
#define MAX_TWO			1002
#define MAX_THREE		1171
#define MAX_FOUR		1526
#define MAX_FIVE		969

#define TMP_IMAGE_PATH "D:\\magData\\tmp\\"
#define TMP_LINE_STATISTIC_PATH "D:\\magData\\tmp\\tmpLineStatistic.txt"
#define TMP_CIRCLE_STATISTIC_PATH "D:\\magData\\tmp\\tmpCirlceStatistic.txt"
