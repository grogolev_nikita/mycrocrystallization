// MySavollaUI.cpp : Defines the entry point for the application.
//

#include "stdafx.h"
#include "MySavollaUI.h"
#include <functional>
#include <iostream>
#include <opencv\cv.h>
#include <opencv2\highgui\highgui.hpp>
#include <opencv2\imgproc\imgproc.hpp>
#include <opencv2\features2d\features2d.hpp>
#include <ctime>
#include "SpotCluster.h"
#include "FernCluster.h"
#include <iomanip>
#include "CSVDataExporter.h"

using namespace cv;
using namespace std;

// Global Variables:
#define MAX_LOADSTRING 100
#define HEADERS_COUNT 22
#define DIRECTIONS_COUNT 8
#define DEFAULT_AREA_UNIT 100
HINSTANCE hInst;                                // current instance
WCHAR szTitle[MAX_LOADSTRING];                  // The title bar text
WCHAR szWindowClass[MAX_LOADSTRING];            // the main window class name
int BUFFER_SIZE = 255;
int winx = 100, winy = 100;
int winxDensity = 200, winyDensity = 200;
float k = 0.1f;
float dR = 128;
int minLength = 400, maxGap = 2;
int rho = 1, theta = 180, thres = 100;
bool isCanny = false;
CvMemStorage* cvLineMemStorage;
CvMemStorage* cvCircleMemStorage;
CvSeq* lines;
uint64_t pxAverageDensity;
uint64_t pxMaxDensity, pxMinDensity;
uint64_t badBlocks;
int arrowDirections[3][3] = 
{
	{-4,  1,  2},
	{-3,  0,  3},
	{-2, -1,  4}
};

char* headers[] = { "x8", "x7", "x6", "x5", "x4", "x3", "x2", "x1", "AverageChainLength", "AverageContourWidth",
"TotalLines", "AverageLineLength", "MaxLineLength", "less700", "less100",
"more1000", "Parallels", "Collusuins", "Total Blobs", "MaxBlobRadius", "MinBlobRadius", "File ID"};

int squareDependingParamsMask[] = {0,0,0,0,0,0,0,0,1,1,1,1,1,1,1,1,1,1,1,1,1,0};

// Forward declarations of functions included in this code module:
ATOM                MyRegisterClass(HINSTANCE hInstance);
BOOL                InitInstance(HINSTANCE, int);
LRESULT CALLBACK    WndProc(HWND, UINT, WPARAM, LPARAM);
INT_PTR CALLBACK    About(HWND, UINT, WPARAM, LPARAM);
LPTSTR				OpenFile(HWND hwnd);

VOID				BinarizeFile(LPTSTR imputFileName, bool bDrawGrid = false);
VOID			    BinarizeFile(Mat& inputMat, bool bDrawGrid = false);
VOID				ClearImage(Mat& mat);
VOID				ClearImageBorders(Mat& mat);
VOID				ClusteriseLines(CvSeq* cvLines, LPTSTR imagePath, vector<double>&);
VOID				drawLines(Mat& mat, CvSeq* cvLines);
VOID				DetectBlobs(LPTSTR imagePath);
VOID				drawGrid(Mat& mat, int cellSize = 1);
VOID				getContours(Mat& mat, vector<double>&);
VOID				convertContourToFreemanChain(const vector<Point> &contour, vector<int> &chain);
double*				analizeContourChain(vector<int> &chain);
double				getAverageContourWidth(vector<Point> contour);
double				getImageEntropy(Mat& image);

CvSeq*				GetLines(LPTSTR imageFilePath);
CvSeq*				GetLines(Mat& mat);
VOID				parseFileNamesString(LPTSTR fileNames, std::vector<LPWSTR> &vec);
std::wstring		getFirstFileName(wchar_t* str);
VOID				GetLinesStatistic(CvSeq* lines, vector<double>&);
int					Classify(int totalLines, int averageLength, int badBlocks);
int					getFileID(wchar_t* fileName);
bool				areCrossing(float x1, float  y1, float  x2, float  y2, float  x3, float  y3, float  x4, float  y4);
int					GetLineLength(int x1, int x2, int y1, int y2);
int					getMatSquare(Mat mat, int pxAreaUnitValue);

template<typename T>
bool compareWithEps(T x, T y, T eps) 
{
	return (abs(x - y) < eps);
}

int classify(int linesCount) 
{
	const int strongSymptomValue = 3;
	const int weakSymptomValue = 1;

	const int dominatingFrequancyIndex = 7;
	const int linesCountIndex = 10;
	const int collisionsCountIndex = 17;

	const int clustersCount = 2;
	vector<int> clustersRating(clustersCount);
	ZeroMemory(&clustersRating[0], clustersCount * sizeof(int));

	if ((linesCount < 0.00001))
		if (true)
			clustersRating[0] += strongSymptomValue;
		else
			clustersRating[1] += strongSymptomValue;
	else
		clustersRating[1] += strongSymptomValue;

	/*
	if ((params[dominatingFrequancyIndex - 1] < 0.17) && (params[collisionsCountIndex] > 20))
		clustersRating[1] += strongSymptomValue;

	if ((params[dominatingFrequancyIndex] > 0.19) && (params[dominatingFrequancyIndex] < 0.2))
		clustersRating[2] = weakSymptomValue;

	if ((params[dominatingFrequancyIndex] > 0.2) && (params[linesCountIndex] < 20))
		clustersRating[3] += weakSymptomValue;

	if ((params[dominatingFrequancyIndex] + params[dominatingFrequancyIndex - 1]) >= 0.4)
		clustersRating[5] += strongSymptomValue;

	if ((clustersRating[0] == 0) && (clustersRating[1] == 0) && (clustersRating[2] == 0)
		&& (clustersRating[3] == 0) && (clustersRating[5] == 0))
		clustersRating[4] += strongSymptomValue;
	*/

	int clusterNumber = 0;
	int maxSymptom = 0;
	for (int i = 0; i < clustersCount; i++)
	{
		if (clustersRating[i] > maxSymptom)
		{
			maxSymptom = clustersRating[i];
			clusterNumber = i;
		}
	}

	return clusterNumber;
}

int exp10(double arg)
{
	if (arg < 1e-8)
		return 0;

	return (int) (log10(fabs(arg)) - 1.0);
}

int APIENTRY wWinMain(_In_ HINSTANCE hInstance,
	_In_opt_ HINSTANCE hPrevInstance,
	_In_ LPWSTR    lpCmdLine,
	_In_ int       nCmdShow)
{
	UNREFERENCED_PARAMETER(hPrevInstance);
	UNREFERENCED_PARAMETER(lpCmdLine);

	// TODO: Place code here.

	// Initialize global strings
	LoadStringW(hInstance, IDS_APP_TITLE, szTitle, MAX_LOADSTRING);
	LoadStringW(hInstance, IDC_MYSAVOLLAUI, szWindowClass, MAX_LOADSTRING);
	MyRegisterClass(hInstance);

	// Perform application initialization:
	if (!InitInstance(hInstance, nCmdShow))
	{
		return FALSE;
	}

	HACCEL hAccelTable = LoadAccelerators(hInstance, MAKEINTRESOURCE(IDC_MYSAVOLLAUI));

	MSG msg;

	// Main message loop:
	while (GetMessage(&msg, nullptr, 0, 0))
	{
		if (!TranslateAccelerator(msg.hwnd, hAccelTable, &msg))
		{
			TranslateMessage(&msg);
			DispatchMessage(&msg);
		}
	}

	return (int)msg.wParam;
}



//
//  FUNCTION: MyRegisterClass()
//
//  PURPOSE: Registers the window class.
//
ATOM MyRegisterClass(HINSTANCE hInstance)
{
	WNDCLASSEXW wcex;

	wcex.cbSize = sizeof(WNDCLASSEX);

	wcex.style = CS_HREDRAW | CS_VREDRAW;
	wcex.lpfnWndProc = WndProc;
	wcex.cbClsExtra = 0;
	wcex.cbWndExtra = 0;
	wcex.hInstance = hInstance;
	wcex.hIcon = LoadIcon(hInstance, MAKEINTRESOURCE(IDI_MYSAVOLLAUI));
	wcex.hCursor = LoadCursor(nullptr, IDC_ARROW);
	wcex.hbrBackground = (HBRUSH)(COLOR_WINDOW + 1);
	wcex.lpszMenuName = MAKEINTRESOURCEW(IDC_MYSAVOLLAUI);
	wcex.lpszClassName = szWindowClass;
	wcex.hIconSm = LoadIcon(wcex.hInstance, MAKEINTRESOURCE(IDI_SMALL));

	return RegisterClassExW(&wcex);
}

//
//   FUNCTION: InitInstance(HINSTANCE, int)
//
//   PURPOSE: Saves instance handle and creates main window
//
//   COMMENTS:
//
//        In this function, we save the instance handle in a global variable and
//        create and display the main program window.
//
BOOL InitInstance(HINSTANCE hInstance, int nCmdShow)
{
	hInst = hInstance; // Store instance handle in our global variable

	HWND hWnd = CreateWindowW(szWindowClass, szTitle, WS_OVERLAPPEDWINDOW,
		CW_USEDEFAULT, 0, CW_USEDEFAULT, 0, nullptr, nullptr, hInstance, nullptr);

	if (!hWnd)
	{
		return FALSE;
	}

	ShowWindow(hWnd, nCmdShow);
	UpdateWindow(hWnd);

	return TRUE;
}

//
//  FUNCTION: WndProc(HWND, UINT, WPARAM, LPARAM)
//
//  PURPOSE:  Processes messages for the main window.
//
//  WM_COMMAND  - process the application menu
//  WM_PAINT    - Paint the main window
//  WM_DESTROY  - post a quit message and return
//
//
LRESULT CALLBACK WndProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
	static LPWSTR fileName;
	static std::vector<LPWSTR> fileNamesVec; 
	static LPWSTR binarizedImagesOutputFolder;
	static HBITMAP hBitmap;
	static HWND hK, hWinX, hWinY, hMinLength, hMaxGap;
	static HWND hRho, hTheta, hThreshold;
	static HWND hCanny;
	static BOOL binarizationFinished;

	switch (message)
	{
	case WM_CREATE:
	{
		CreateWindow(_T("STATIC"), _T("winX"), WS_VISIBLE | WS_CHILD,
			10, 10, 100, 20, hWnd, NULL, NULL, NULL);
		CreateWindow(_T("STATIC"), _T("winY"), WS_VISIBLE | WS_CHILD,
			120, 10, 100, 20, hWnd, NULL, NULL, NULL);
		hWinX = CreateWindow(_T("EDIT"), NULL, WS_BORDER | WS_VISIBLE | WS_CHILD | ES_LEFT | ES_MULTILINE,
			10, 30, 100, 20, hWnd, NULL, NULL, NULL);
		hWinY = CreateWindow(_T("EDIT"), NULL, WS_BORDER | WS_VISIBLE | WS_CHILD | ES_LEFT | ES_MULTILINE,
			120, 30, 100, 20, hWnd, NULL, NULL, NULL);

		CreateWindow(_T("STATIC"), _T("k"), WS_VISIBLE | WS_CHILD,
			10, 60, 100, 20, hWnd, NULL, NULL, NULL);
		hK = CreateWindow(_T("EDIT"), NULL, WS_BORDER | WS_VISIBLE | WS_CHILD | ES_LEFT | ES_MULTILINE,
			10, 80, 100, 20, hWnd, NULL, NULL, NULL);

		CreateWindow(_T("STATIC"), _T("minLength"), WS_VISIBLE | WS_CHILD,
			10, 110, 100, 20, hWnd, NULL, NULL, NULL);
		CreateWindow(_T("STATIC"), _T("maxGap"), WS_VISIBLE | WS_CHILD,
			120, 110, 100, 20, hWnd, NULL, NULL, NULL);
		hMinLength = CreateWindow(_T("EDIT"), NULL, WS_BORDER | WS_VISIBLE | WS_CHILD | ES_LEFT | ES_MULTILINE,
			10, 130, 100, 20, hWnd, NULL, NULL, NULL);
		hMaxGap = CreateWindow(_T("EDIT"), NULL, WS_BORDER | WS_VISIBLE | WS_CHILD | ES_LEFT | ES_MULTILINE,
			120, 130, 100, 20, hWnd, NULL, NULL, NULL);

		CreateWindow(_T("STATIC"), _T("rho"), WS_VISIBLE | WS_CHILD,
			10, 160, 100, 20, hWnd, NULL, NULL, NULL);
		CreateWindow(_T("STATIC"), _T("theta "), WS_VISIBLE | WS_CHILD,
			120, 160, 100, 20, hWnd, NULL, NULL, NULL);
		CreateWindow(_T("STATIC"), _T("threshold "), WS_VISIBLE | WS_CHILD,
			230, 160, 100, 20, hWnd, NULL, NULL, NULL);
		hRho = CreateWindow(_T("EDIT"), NULL, WS_BORDER | WS_VISIBLE | WS_CHILD | ES_LEFT | ES_MULTILINE,
			10, 180, 100, 20, hWnd, NULL, NULL, NULL);
		hTheta = CreateWindow(_T("EDIT"), NULL, WS_BORDER | WS_VISIBLE | WS_CHILD | ES_LEFT | ES_MULTILINE,
			120, 180, 100, 20, hWnd, NULL, NULL, NULL);
		hThreshold = CreateWindow(_T("EDIT"), NULL, WS_BORDER | WS_VISIBLE | WS_CHILD | ES_LEFT | ES_MULTILINE,
			230, 180, 100, 20, hWnd, NULL, NULL, NULL);

		CreateWindow(_T("STATIC"), _T("canny/inverse"), WS_VISIBLE | WS_CHILD,
			10, 210, 100, 20, hWnd, NULL, NULL, NULL);
		hCanny = CreateWindow(_T("BUTTON"), NULL, WS_VISIBLE | WS_CHILD | BS_CHECKBOX,
			10, 230, 100, 20, hWnd, (HMENU)IDC_CANNY_CHECKBOX, ((LPCREATESTRUCT)lParam)->hInstance, NULL);

		CreateWindow(_T("BUTTON"), _T("BINARIZE"), WS_VISIBLE | WS_CHILD,
			10, 260, 100, 40, hWnd,
			(HMENU)IDC_BINARIZE_BUTTON, NULL, NULL);

		CreateWindow(_T("BUTTON"), _T("GET LINES"), WS_VISIBLE | WS_CHILD,
			120, 260, 100, 40, hWnd,
			(HMENU)IDC_GET_LINES_BUTTON, NULL, NULL);

		CreateWindow(_T("BUTTON"), _T("CLEAR LINES"), WS_VISIBLE | WS_CHILD,
			120, 310, 100, 40, hWnd,
			(HMENU)IDC_CLEAR_LINES_BUTTON, NULL, NULL);
		CreateWindow(_T("BUTTON"), _T("GET FERNS"), WS_VISIBLE | WS_CHILD,
			10, 310, 100, 40, hWnd,
			(HMENU)IDC_GET_FERNS_BUTTON, NULL, NULL);
		CreateWindow(_T("BUTTON"), _T("FULL IMAGE ANALYSIS"), WS_VISIBLE | WS_CHILD,
			300, 500, 200, 100, hWnd,
			(HMENU)IDC_FULL_IMAGE_ANALYSIS, NULL, NULL);

		SetWindowText(hWinX, L"250");
		SetWindowText(hWinY, L"250");

		SetWindowText(hK, L"0.001");

		SetWindowText(hMinLength, L"200");
		SetWindowText(hMaxGap, L"5");

		SetWindowText(hRho, L"1");
		SetWindowText(hTheta, L"360");
		SetWindowText(hThreshold, L"50");
	}
	break;
	case WM_COMMAND:
	{
		int wmId = LOWORD(wParam);
		switch (wmId)
		{
		case IDM_ABOUT:
			DialogBox(hInst, MAKEINTRESOURCE(IDD_ABOUTBOX), hWnd, About);
			break;
		case IDM_EXIT:
			DestroyWindow(hWnd);
			break;
		case ID_FILE_OPENFILE:
		{
			TCHAR* result = OpenFile(hWnd);
			parseFileNamesString(result, fileNamesVec);

			delete [] result;

			hBitmap = (HBITMAP)LoadImage(hInst, fileName, IMAGE_BITMAP, 0, 0, LR_LOADFROMFILE);
			InvalidateRect(hWnd, NULL, TRUE);
		}
		break;
		case IDC_GET_FERNS_BUTTON:
		{
			LPTSTR imagePath = OpenFile(hWnd);
			size_t i;
			char *cv_imputFileName = new char[BUFFER_SIZE];
			wcstombs_s(&i, cv_imputFileName, BUFFER_SIZE, imagePath, BUFFER_SIZE);

			Mat inputMat = imread(cv_imputFileName, 1);
			// getContours(inputMat);
		}
		break;
		case IDC_FULL_IMAGE_ANALYSIS:
		{
			vector<double*> csvTable;
			LPTSTR imagePath = OpenFile(hWnd);
			int minNumberOrders[HEADERS_COUNT];
			ZeroMemory(minNumberOrders, HEADERS_COUNT * sizeof(int));

			auto t = std::time(nullptr);
			auto tm = *std::localtime(&t);
			ostringstream oss;
			oss << put_time(&tm, "%d-%m-%Y %H-%M-%S");

			string fileNameWithDate = "E:\\SampleData\\frequency";
			fileNameWithDate += oss.str() + ".csv";

			CSVDataExporter csvEx(fileNameWithDate.c_str(), headers, HEADERS_COUNT);
			CvSeq* localLines;
			parseFileNamesString(imagePath, fileNamesVec);
			for (int i = 0; i < fileNamesVec.size(); i++) 
			{
				size_t j;
				char *cv_imputFileName = new char[BUFFER_SIZE];
				wcstombs_s(&j, cv_imputFileName, BUFFER_SIZE, fileNamesVec[i], BUFFER_SIZE);

				Mat inputMat = imread(cv_imputFileName, IMREAD_GRAYSCALE);
				Mat originalMat(inputMat);
				int matSqure = getMatSquare(inputMat, DEFAULT_AREA_UNIT);

				BinarizeFile(inputMat);
				localLines = GetLines(inputMat);

				vector<double> linesParams;
				GetLinesStatistic(localLines, linesParams);
				//vector<double> countoursParams;
				//getContours(inputMat, countoursParams);
				//vector<double> blobsParams;
				//ClusteriseLines(localLines, fileNamesVec[i], blobsParams);

//				transform(linesParams.begin(), linesParams.end(), linesParams.begin(), bind(divides<int>(), placeholders::_1, matSqure));

				vector<double> params;
				//params.insert(params.end(), countoursParams.begin(), countoursParams.end());
				//params.insert(params.end(), linesParams.begin(), linesParams.end());			
				//params.insert(params.end(), blobsParams.begin(), blobsParams.end());
				int fileID = getFileID(fileNamesVec[i]);
				params.push_back(fileID);
				int liensCount = linesParams[0];
				int cluster = classify(liensCount);
				params.push_back(cluster);
				
				/*
				for (int i = 0; i < HEADERS_COUNT; i++)
				{
					if (!squareDependingParamsMask[i])
						continue;

					params[i] = params[i] / (double)matSqure;
					int exp = exp10(params[i]);

					if (exp < minNumberOrders[i])
						minNumberOrders[i] = exp;
				}
				*/
				double* csvRow = new double[2];
				csvRow[0] = params[0];
				csvRow[1] = params[1];
				//memcpy(csvRow, &params[0], HEADERS_COUNT * sizeof(double));
				csvEx.addFieldsToRow(csvRow, 2);
				csvEx.closeRow();
				//csvTable.push_back(csvRow);
				delete[] cv_imputFileName;
			}

			/*
			for (int i = 0; i < csvTable.size(); i++)
			{
				for (int j = 0; j < HEADERS_COUNT; j++)
				{
					if (!squareDependingParamsMask[j])
						continue;

					if (minNumberOrders[j] >= 0)
						continue;

					int multiplier = pow(10, abs(minNumberOrders[j]));
					csvTable[i][j] *= multiplier;
				}

				csvEx.addFieldsToRow(csvTable[i], HEADERS_COUNT);
				csvEx.closeRow();
			}
			*/
		}
		break;
		case IDC_BINARIZE_BUTTON:
		{
			wchar_t* text = new wchar_t[BUFFER_SIZE];
			int tmpInt = GetWindowTextLength(hK) + 1;
			GetWindowText(hK, &text[0], tmpInt);
			k = _ttof(text);
			tmpInt = GetWindowTextLength(hWinX) + 1;
			GetWindowText(hWinX, &text[0], tmpInt);
			winx = _ttoi(text);
			tmpInt = GetWindowTextLength(hWinY) + 1;
			GetWindowText(hWinY, &text[0], tmpInt);
			winy = _ttoi(text);
			delete[] text;
			
			if (k > 0. && winx != 0 && winy != 0)
			{
				for (int i = 0; i < fileNamesVec.size(); i++)
				{
					BinarizeFile(fileNamesVec[i]);
				}
				
				binarizationFinished = true;
				// hBitmap = (HBITMAP)LoadImage(hInst, /* some bitmap */, IMAGE_BITMAP, 0, 0, LR_LOADFROMFILE);
				InvalidateRect(hWnd, NULL, TRUE);
			}
		}
		break;
		case IDC_GET_LINES_BUTTON:
		{
			LPTSTR imagePath = OpenFile(hWnd);

			wchar_t* text = new wchar_t[BUFFER_SIZE];
			int tmpInt = GetWindowTextLength(hMinLength) + 1;
			GetWindowText(hMinLength, &text[0], tmpInt);
			minLength = _ttoi(text);
			tmpInt = GetWindowTextLength(hMaxGap) + 1;
			GetWindowText(hMaxGap, &text[0], tmpInt);
			maxGap = _ttoi(text);

			tmpInt = GetWindowTextLength(hRho) + 1;
			GetWindowText(hRho, &text[0], tmpInt);
			rho = _ttoi(text);
			tmpInt = GetWindowTextLength(hTheta) + 1;
			GetWindowText(hTheta, &text[0], tmpInt);
			theta = _ttoi(text);
			tmpInt = GetWindowTextLength(hThreshold) + 1;
			GetWindowText(hThreshold, &text[0], tmpInt);
			thres = _ttoi(text);

			lines = GetLines(imagePath);
			// DetectBlobs(imagePath);
			//ClusteriseLines(lines, imagePath);
			// GetLinesStatistic(lines, _T(TMP_LINE_STATISTIC_PATH));

			InvalidateRect(hWnd, NULL, TRUE);
		}
		break;
		case IDC_CLEAR_LINES_BUTTON:
		{
			wchar_t* text = new wchar_t[BUFFER_SIZE];
			int tmpInt = GetWindowTextLength(hK) + 1;
			GetWindowText(hK, &text[0], tmpInt);
			k = _ttof(text);
			tmpInt = GetWindowTextLength(hWinX) + 1;
			GetWindowText(hWinX, &text[0], tmpInt);
			winx = _ttoi(text);
			tmpInt = GetWindowTextLength(hWinY) + 1;
			GetWindowText(hWinY, &text[0], tmpInt);
			winy = _ttoi(text);

			tmpInt = GetWindowTextLength(hMinLength) + 1;
			GetWindowText(hMinLength, &text[0], tmpInt);
			minLength = _ttoi(text);
			tmpInt = GetWindowTextLength(hMaxGap) + 1;
			GetWindowText(hMaxGap, &text[0], tmpInt);
			maxGap = _ttoi(text);

			tmpInt = GetWindowTextLength(hRho) + 1;
			GetWindowText(hRho, &text[0], tmpInt);
			rho = _ttoi(text);
			tmpInt = GetWindowTextLength(hTheta) + 1;
			GetWindowText(hTheta, &text[0], tmpInt);
			theta = _ttoi(text);
			tmpInt = GetWindowTextLength(hThreshold) + 1;	
			GetWindowText(hThreshold, &text[0], tmpInt);
			thres = _ttoi(text);
			delete[] text;

			size_t dummy;
			int totalImages[2][6] = { {19, 9, 26, 30, 8, 1}, {21, 49, 62, 44, 3, 1} };
			int bufferSize = 5;
			char *groupBuffer = new char[bufferSize];
			char *nameBuffer = new char[bufferSize];
			char *fullname = new char[255];
			wchar_t wFullName[255];
			wchar_t wFullStatisticName[255];

			for (int newData = 0; newData < 2; newData++)
			{
				for (int groupNumber = 0; groupNumber < 6; groupNumber++)
				{
					_itoa_s(groupNumber, groupBuffer, bufferSize, 10);
					for (int i = 1; i <= totalImages[newData][groupNumber]; i++)
					{
						_itoa_s(i, nameBuffer, bufferSize, 10);
						strcpy_s(fullname, 255, "D:\\magData\\");
						if (newData > 0)
						{
							strcat_s(fullname, 255, "new\\");
						}
						strcat_s(fullname, 255, groupBuffer);
						strcat_s(fullname, 255, "\\");
						strcat_s(fullname, 255, groupBuffer);
						strcat_s(fullname, 255, "_");
						strcat_s(fullname, 255, nameBuffer);
						strcat_s(fullname, 255, ".jpg");
						mbstowcs_s(&dummy, wFullName, fullname, strlen(fullname) + 1);

						strcpy_s(fullname, 255, "D:\\magData\\tmp\\Group ");
						strcat_s(fullname, 255, groupBuffer);
						strcat_s(fullname, 255, " Line Statistic.txt");
						mbstowcs_s(&dummy, wFullStatisticName, fullname, strlen(fullname) + 1);

						BinarizeFile(wFullName);
						if (binarizationFinished)
						{
							lines = GetLines(_T(TMP_IMAGE_PATH));
							//GetLinesStatistic(lines);
						}
					}
				}
			}
			exit(0);
		}
		break;
		case IDC_CANNY_CHECKBOX:
		{
			BOOL checked = IsDlgButtonChecked(hWnd, IDC_CANNY_CHECKBOX);
			if (checked)
			{
				CheckDlgButton(hWnd, IDC_CANNY_CHECKBOX, BST_UNCHECKED);
				isCanny = false;
			}
			else
			{

			}
			{
				CheckDlgButton(hWnd, IDC_CANNY_CHECKBOX, BST_CHECKED);
				isCanny = true;
			}
		}
		break;
		default:
			return DefWindowProc(hWnd, message, wParam, lParam);
		}
	}
	break;
	case WM_PAINT:
	{
		PAINTSTRUCT ps;
		HDC hdc = BeginPaint(hWnd, &ps);

		BITMAP 			bitmapForImage;
		HDC 			hdcForImage, hdcForLines;
		HGDIOBJ 		bitmapForLines, oldBitmapForImage, oldBitmapForLines;
		GetObject(hBitmap, sizeof(bitmapForImage), &bitmapForImage);

		hdcForImage = CreateCompatibleDC(hdc);
		oldBitmapForImage = SelectObject(hdcForImage, hBitmap);

		bitmapForLines = CreateCompatibleBitmap(hdcForImage, bitmapForImage.bmWidth, bitmapForImage.bmHeight);

		hdcForLines = CreateCompatibleDC(hdcForImage);
		oldBitmapForLines = (HBITMAP)SelectObject(hdcForLines, bitmapForLines);
		BitBlt(hdcForLines, 0, 0, bitmapForImage.bmWidth, bitmapForImage.bmHeight, hdcForImage, 0, 0, SRCCOPY);

		if (lines != NULL)
		{
			HGDIOBJ	hOldPen;
			HPEN hPen;
			hPen = CreatePen(PS_SOLID, 5, RGB(255, 0, 0));
			hOldPen = SelectObject(hdcForLines, hPen);
			for (int i = 0; i < lines->total; i++)
			{
				CvPoint* line = (CvPoint*)cvGetSeqElem(lines, i);
				MoveToEx(hdcForLines, line[0].x, line[0].y, NULL);
				LineTo(hdcForLines, line[1].x, line[1].y);
			}
			DeleteObject(SelectObject(hdcForLines, hOldPen));
		}
		RECT rect;
		GetClientRect(hWnd, &rect);
		StretchBlt(hdc, 0, 0, rect.right, rect.bottom, hdcForLines, 0, 0, bitmapForImage.bmWidth, bitmapForImage.bmHeight, SRCCOPY);

		SelectObject(hdcForLines, oldBitmapForLines);
		DeleteDC(hdcForLines);

		SelectObject(hdcForImage, oldBitmapForImage);
		DeleteDC(hdcForImage);

		EndPaint(hWnd, &ps);

	}
	break;
	case WM_DESTROY:
		PostQuitMessage(0);
		break;
	default:
		return DefWindowProc(hWnd, message, wParam, lParam);
	}
	return 0;
}

// Message handler for about box.
INT_PTR CALLBACK About(HWND hDlg, UINT message, WPARAM wParam, LPARAM lParam)
{
	UNREFERENCED_PARAMETER(lParam);
	switch (message)
	{
	case WM_INITDIALOG:
		return (INT_PTR)TRUE;

	case WM_COMMAND:
		if (LOWORD(wParam) == IDOK || LOWORD(wParam) == IDCANCEL)
		{
			EndDialog(hDlg, LOWORD(wParam));
			return (INT_PTR)TRUE;
		}
		break;
	}
	return (INT_PTR)FALSE;
}

LPTSTR OpenFile(HWND hwnd)
{
	OPENFILENAME ofn;       // common dialog box structure
	WCHAR szFile[8192];       // buffer for file name
	HANDLE hf;              // file handle
	TCHAR* buf = new TCHAR[8192];
							// Initialize OPENFILENAME
	ZeroMemory(&ofn, sizeof(ofn));
	ofn.lStructSize = sizeof(ofn);
	ofn.hwndOwner = hwnd;
	ofn.lpstrFile = szFile;
	ofn.lpstrFile[0] = '\0';
	ofn.nMaxFile = sizeof(szFile);
	ofn.lpstrFilter = L"Image\0*.jpg;*.jpeg;*.png;*.bmp;\0\0";
	ofn.nFilterIndex = 1;
	ofn.lpstrFileTitle = NULL;
	ofn.nMaxFileTitle = 0;
	ofn.lpstrInitialDir = L"E:\\";
	ofn.Flags = OFN_PATHMUSTEXIST | OFN_FILEMUSTEXIST | OFN_ALLOWMULTISELECT;

	if (GetOpenFileName(&ofn) == TRUE)
		_tcscpy_s(buf, 8192, ofn.lpstrFile);
	else
		buf[0] = 0;

	return buf;
}

int getFileID(wchar_t* fileName)
{
	wchar_t drive[256];
	wchar_t dir[256];
	wchar_t fname[256];
	wchar_t ext[256];
	_wsplitpath(fileName, drive, dir, fname, ext);

	return _wtoi(fname);
}

VOID convertContourToFreemanChain(const vector<Point> &contour, vector<int> &chain)
{
	/*
	(Directions of arrows : number)
		UP : 1;
		UP-RIGHT : 2;
		RIGHT : 3;
		DOWN-RIGHT : 4;
		DOWN : -1;
		DOWN-LEFT : -2;
		LEFT : -3;
		LEFT-UP : -4;
	*/
	for (int i = 0; i < contour.size() - 1; i++)
	{
		int xSub = contour[i + 1].x - contour[i].x;
		int ySub = contour[i + 1].y - contour[i].y;

		chain.push_back(arrowDirections[abs(ySub - 1)][xSub + 1]);
	}
	chain.push_back(0);
}

std::wstring getFirstFileName(wchar_t* str)
{
	std::wstring fileName;
	int i = 0;

	while ( (str[i] != ' ') )
	{
		if ( str[i] == 0 )
			break;

		fileName.push_back(str[i]);
		i++;
	}
	return fileName;
}

VOID getContours(Mat& mat, vector<double>& paramsPart)
{
	paramsPart.clear();
	int cxMinContourSize = 100;
	int thresh = 255;
	//cvtColor(mat, grayMat, CV_BGR2GRAY);
	//blur(grayMat, grayMat, Size(3, 3));

	vector<vector<Point>> contours;
	//Canny(grayMat, cannyMat, thresh, thresh * 2, 3);
	Mat cMat = mat;
	//cv::imshow("mat", mat);
	findContours(cMat, contours, RETR_TREE, CV_CHAIN_APPROX_NONE);

	double* middleFrequency = new double[DIRECTIONS_COUNT + 1];
	memset(middleFrequency, 0, sizeof(double) * (DIRECTIONS_COUNT + 1));
	int validContoursCount = 0;
	double averageContourWidth = 0.0;
	for (int i = 0; i < contours.size(); i++)
	{
		if (contours[i].size() < cxMinContourSize)
			continue;

		averageContourWidth += getAverageContourWidth(contours[i]);
		validContoursCount++;
		vector<int> freemanChain;
		convertContourToFreemanChain(contours[i], freemanChain);
		double* frequency = analizeContourChain(freemanChain);
		std::sort(frequency, frequency + DIRECTIONS_COUNT);
		for (int j = 0; j < DIRECTIONS_COUNT; j++)
		{
			middleFrequency[j] += frequency[j];
		}
		middleFrequency[DIRECTIONS_COUNT] += contours[i].size();
		delete[] frequency;
	}

	for (int i = 0; i < DIRECTIONS_COUNT + 1; i++)
		paramsPart.push_back(middleFrequency[i] / validContoursCount);

	paramsPart.push_back(averageContourWidth / validContoursCount);
	imshow("cMat", cMat);
	destroyWindow("cMat");
	delete[] middleFrequency;
}

double getImageEntropy(Mat& image)
{
	if (image.channels() == 3) cvtColor(image, image, CV_BGR2GRAY);
	int histSize = 256;

	float range[] = { 0, 256 };
	const float* histRange = { range };

	Mat hist;
	calcHist(&image, 1, 0, Mat(), hist, 1, &histSize, &histRange);
	hist /= image.total();

	Mat logP;
	log(hist, logP);

	return -1 * sum(hist.mul(logP)).val[0];
}

double getAverageContourWidth(vector<Point> contour) 
{
	double averageWidth = 0.0;
	int half = contour.size() >> 1;
	for (int i = 0; i < half; i++)
	{
		averageWidth += GetLineLength(contour[i].x, contour[contour.size() - i - 1].x,
			contour[i].y, contour[contour.size() - i - 1].y);
	}
	
	return averageWidth / contour.size();
}

VOID parseFileNamesString(LPTSTR fileNames, std::vector<LPWSTR> &vec)
{
	vec.clear();
	wchar_t* str = fileNames;
	std::wstring directoryName = getFirstFileName(str);
	str += (directoryName.length() + 1);

	if (!*(str - 1)) 
	{
		wchar_t* res = new wchar_t[directoryName.size() + 1];
		_tcscpy(res, directoryName.c_str());
		vec.push_back((LPWSTR)res);
	}

	while ( *(str - 1) ) 
	{
		std::wstring fileName = getFirstFileName(str);
		str += ( fileName.length() + 1 );
		
		std::wstring result = directoryName + fileName;
		wchar_t* res = new wchar_t[result.size() + 1];
		_tcscpy(res, result.c_str());
		vec.push_back( (LPWSTR) res );
	}
}

VOID BinarizeFile(LPTSTR inputFileName, bool bDrawGrid)
{
	int gaussianKernelSizeX = 3;
	int gaussianKernelSizeY = 3;

	time_t currentTime = time(0);
	struct tm * currentDateAndTime = localtime(&currentTime);

	size_t i;
	char *cv_imputFileName = new char[BUFFER_SIZE];
	wcstombs_s(&i, cv_imputFileName, BUFFER_SIZE, inputFileName, BUFFER_SIZE);

	Mat inputMat = imread(cv_imputFileName, IMREAD_GRAYSCALE);
	delete[] cv_imputFileName;

	if ((inputMat.rows > 0) && (inputMat.cols > 0))
	{
		Mat outputMat(inputMat.rows, inputMat.cols, CV_8U);
		Sauvola(inputMat, outputMat, winx, winy, k, dR);

		// cv::GaussianBlur(inputMat, outputMat, Size(gaussianKernelSizeX, gaussianKernelSizeY), 0.0, 0.0);
		// ClearImageBorders(outputMat);
		// ClearImage(outputMat);
		if (bDrawGrid) 
			drawGrid(outputMat);
		std::string tmpFileName(std::to_string(currentDateAndTime->tm_year + 1900) + '-' + std::to_string(currentDateAndTime->tm_mon + 1) + '-' + std::to_string(currentDateAndTime->tm_mday) +
			'-' + std::to_string(currentDateAndTime->tm_hour) + '-' + std::to_string(currentDateAndTime->tm_min) + '-' + std::to_string(currentDateAndTime->tm_sec));
		std::string tmpPathString = TMP_IMAGE_PATH + tmpFileName + ".bmp";
		cv::String tmpPath(tmpPathString.c_str() );

		imwrite(tmpPath, outputMat);
	}
}

VOID BinarizeFile(Mat& inputMat, bool bDrawGrid)
{
	if ((inputMat.rows > 0) && (inputMat.cols > 0))
	{
		Mat outputMat(inputMat.rows, inputMat.cols, CV_8U);
		Sauvola(inputMat, outputMat, winx, winy, k, dR);

		if (bDrawGrid)
			drawGrid(outputMat);

		inputMat = outputMat;
	}
}

CvSeq* GetLines(Mat& mat)
{
	IplImage* src = new IplImage(mat);
	IplImage* not = 0;
	IplImage* canny = 0;

	not = cvCreateImage(cvGetSize(src), 8, 1);
	canny = cvCreateImage(cvGetSize(src), 8, 1);

	cvNot(src, not);
	cvCanny(src, canny, 50, 200, 3);
	if (cvLineMemStorage == NULL)
	{
		cvLineMemStorage = cvCreateMemStorage();
	}

	int matSquare = getMatSquare(mat, DEFAULT_AREA_UNIT);
	int minLineLength = (minLength * matSquare) / 100;
	if (minLineLength >= 400)
		minLineLength /= 2;

	if (isCanny)
	{
		return cvHoughLines2(canny, cvLineMemStorage, CV_HOUGH_PROBABILISTIC, rho, CV_PI / theta, thres, minLineLength, maxGap);
	}
	else
	{
		return cvHoughLines2(not, cvLineMemStorage, CV_HOUGH_PROBABILISTIC, rho, CV_PI / theta, thres, minLineLength, maxGap);
	}

}

CvSeq* GetLines(LPTSTR imageFilePath)
{
	size_t   i;
	char *cv_imageFilePath = new char[BUFFER_SIZE];
	wcstombs_s(&i, cv_imageFilePath, BUFFER_SIZE, imageFilePath, BUFFER_SIZE);

	IplImage* src = 0;
	IplImage * not = 0;
	IplImage* canny = 0;

	src = cvLoadImage(cv_imageFilePath, CV_LOAD_IMAGE_GRAYSCALE);
	delete[] cv_imageFilePath;

	not = cvCreateImage(cvGetSize(src), 8, 1);
	canny = cvCreateImage(cvGetSize(src), 8, 1);

	cvNot(src, not);
	cvCanny(src, canny, 50, 200, 3);
	if (cvLineMemStorage == NULL)
	{
		cvLineMemStorage = cvCreateMemStorage();
	}

	if (isCanny)
	{
		return cvHoughLines2(canny, cvLineMemStorage, CV_HOUGH_PROBABILISTIC, rho, CV_PI / theta, thres, minLength, maxGap);
	}
	else
	{
		return cvHoughLines2(not, cvLineMemStorage, CV_HOUGH_PROBABILISTIC, rho, CV_PI / theta, thres, minLength, maxGap);
	}
}

VOID GetLinesStatistic(CvSeq* lines, vector<double>& paramsPart)
{
	paramsPart.clear();
	int avLength = 0;
	int maxLength = 0;
	int less700 = 0;
	int less1000 = 0;
	int more1000 = 0;

	for (int i = 0; i < lines->total; i++)
	{
		CvPoint* currentLine = (CvPoint*)cvGetSeqElem(lines, i);
		int currentLength = GetLineLength(currentLine[0].x, currentLine[1].x, currentLine[0].y, currentLine[1].y);
		avLength += currentLength;
		if (maxLength < currentLength)
		{
			maxLength = currentLength;
		}
		if (currentLength < 700)
		{
			less700++;
		}
		else if (currentLength < 1000)
		{
			less1000++;
		}
		else
		{
			more1000++;
		}
	}

	int collisions = 0;
	for (int i = 0; i < lines->total; i++)
	{
		CvPoint* currentLine = (CvPoint*)cvGetSeqElem(lines, i);

		for (int j = i + 1; j < lines->total; j++)
		{
			CvPoint* tmpLine = (CvPoint*)cvGetSeqElem(lines, j);
			if (areCrossing(currentLine[0].x, currentLine[0].y, currentLine[1].x, currentLine[1].y, tmpLine[0].x, tmpLine[0].y, tmpLine[1].x, tmpLine[1].y))
			{
				collisions++;
			}
		}
	}

	int parallels = 0;
	for (int i = 0; i < lines->total; i++)
	{
		CvPoint* currentLine = (CvPoint*)cvGetSeqElem(lines, i);
		int currentLength = GetLineLength(currentLine[0].x, currentLine[1].x, currentLine[0].y, currentLine[1].y);

		if (currentLength > 850)
		{
			float k1 = INFINITE;
			if (currentLine[1].x - currentLine[0].x != 0)
			{
				k1 = ((float)currentLine[1].y - (float)currentLine[0].y) / ((float)currentLine[1].x - (float)currentLine[0].x);
			}
			for (int j = i + 1; j < lines->total; j++)
			{
				CvPoint* tmpLine = (CvPoint*)cvGetSeqElem(lines, j);
				int tmpLength = GetLineLength(tmpLine[0].x, tmpLine[1].x, tmpLine[0].y, tmpLine[1].y);
				if (tmpLength > 850)
				{
					float k2 = INFINITE;
					if (tmpLine[1].x - tmpLine[0].x != 0)
					{
						k2 = ((float)tmpLine[1].y - (float)tmpLine[0].y) / ((float)tmpLine[1].x - (float)tmpLine[0].x);
					}
					if ((k1*k2 >= 0 && abs(k2) > abs(k1) * 0.75 && abs(k2) < abs(k1) * 1.25) || (k1 == INFINITE && k2 == INFINITE))
					{
						parallels++;
						tmpLine[0].x = 0;
						tmpLine[0].y = 0;
						tmpLine[1].x = 0;
						tmpLine[1].y = 0;
					}
				}
			}
		}
	}

	if (lines->total != 0)
	{
		avLength = avLength / lines->total;
	}

	//int group = Classify(lines->total, avLength, badBlocks);


	paramsPart.push_back((double) lines->total);	// << ';'; // "Total : " << 
	paramsPart.push_back((double) avLength);		// << ';'; // "Average Length : " << 
	paramsPart.push_back((double) maxLength);	// << ';'; // "Max Length : " << 
	paramsPart.push_back((double) less700);		// << ';'; // "Less than 700 : " << 
	paramsPart.push_back((double) less1000);		// << ';'; // "Less than 1000 : " << 
	paramsPart.push_back((double) more1000);		// << ';'; // "More than 1000 : " << 
	paramsPart.push_back((double) parallels);	// << ';'; // "Parallels : " << 
	paramsPart.push_back((double) collisions);	// / 2 << ';'; // "Collisions : " << 

	int total[5] = { 360, 701, 651, 779, 867 };
	int average[5] = { 134, 142, 186, 213, 265 };
	int max[5] = { 308, 455, 1002, 1171, 1526 };

	int totalDiff = abs(lines->total - total[0]);
	int averageDiff = abs(avLength - average[0]);
	int maxDiff = abs(maxLength - max[0]);

	int totalGroup = 0;
	int averageGroup = 0;
	int maxGroup = 0;

	for (int i = 1; i < 5; i++)
	{
		int diff = abs(lines->total - total[i]);
		if (diff < totalDiff)
		{
			totalDiff = diff;
			averageGroup = i;
		}
		diff = abs(avLength - average[i]);
		if (diff < averageDiff)
		{
			averageDiff = diff;
			totalGroup = i;
		}
		diff = abs(maxLength - max[i]);
		if (diff < maxDiff)
		{
			maxDiff = diff;
			maxGroup = i;
		}
	}
}

double* analizeContourChain(vector<int> &chain) 
{
	int arrows[] = { -4, -3, -2, -1, 1, 2, 3, 4 };
	double* frequency = new double[DIRECTIONS_COUNT];
	memset(frequency, 0, DIRECTIONS_COUNT * sizeof(double));

	for (int i = 0; i < chain.size(); i++)
	{
		int arrow = chain[i];
		int b = (int) (arrow > 0);
		frequency[arrow + 4 - b]++;	
	}

	for (int i = 0; i < DIRECTIONS_COUNT; i++)
	{
		frequency[i] /= chain.size();
	}

	return frequency;
}

int getMatSquare(Mat mat, int pxAreaUnitValue) 
{
	int width = mat.cols / pxAreaUnitValue;
	int height = mat.rows / pxAreaUnitValue;

	int square = width * height;
	return (square == 0) ? 1 : square;
}


int Classify(int totalLines, int averageLength, int badBlocks) {
	if (totalLines < 35.5) 
	{
		if (totalLines < 10.5) 
		{
			return 0;
		}
		else 
		{
			if (totalLines < 20.5) 
			{
				if (badBlocks < 14.5) 
				{
					if (averageLength < 237.5) 
					{
						return 0;
					}
					else 
					{
						return 1;
					}
				}
				else 
				{
					return 1;
				}
			}
			else 
			{
				return 1;
			}
		}
	}
	else 
	{
		if (totalLines < 69.5) 
		{
			if (averageLength < 269.5) 
			{
				return 1;
			}
			else 
			{
				if (badBlocks < 4.5) 
				{
					if (averageLength < 294.5) 
					{
						if (totalLines < 43)
						{
							return 23;
						}
						else 
						{
							return 1;
						}
					}
					else 
					{
						return 23;
					}
				}
				else 
				{
					return 23;
				}
			}
		}
		else 
		{
			return 23;
		}
	}
}

bool areCrossing(float x1, float  y1, float  x2, float  y2, float  x3, float  y3, float  x4, float  y4)
{
	double k1 = (y2 - y1) / (x2 - x1);
	double k2 = (y4 - y3) / (x4 - x3);
	if (k1*k2 >= 0)
	{
		return false;
	}

	float ux = ((x4 - x3)*(y1 - y3) - (y4 - y3)*(x1 - x3)) / ((y4 - y3)*(x2 - x1) - (x4 - x3)*(y2 - y1));
	float uy = ((x2 - x1)*(y1 - y3) - (y2 - y1)*(x1 - x3)) / ((y4 - y3)*(x2 - x1) - (x4 - x3)*(y2 - y1));

	float xc = x1 + ux*(x2 - x1);
	float yc = y1 + uy*(y2 - y1);

	if (x2 < x1)
	{
		float tmp = x2;
		x2 = x1;
		x1 = tmp;
	}
	if (y2 < y1)
	{
		float tmp = y2;
		y2 = y1;
		y1 = tmp;
	}
	if (x4 < x3)
	{
		float tmp = x4;
		x4 = x3;
		x3 = tmp;
	}
	if (y4 < y3)
	{
		float tmp = y4;
		y4 = y3;
		y3 = tmp;
	}
	if ((x1 < xc && xc < x2) && (y1 < yc && yc < y2) && (x3 < xc && xc < x4) && (y3 < yc && yc < y4))
	{
		return true;
	}

	return false;
}

VOID ClearImageTest(Mat& mat)
{
	for (int i = 0; i < mat.rows; i++)
	{
		for (int j = 0; j < mat.cols; j++)
		{
				int currentDensity = 0;
				unsigned char px = mat.at<unsigned char>(i, j);
				mat.at<unsigned char>(i, j) = (px == 255) ? 255 : 0;
		}
	}
}

VOID ClusteriseLines(CvSeq* lines)
{
	float eps = 2;
	int minSpotLinesDencity = 50;

	DWORD cxlinesCount = lines->total;
	CvPoint* *linesMiddlePoints = new CvPoint*[cxlinesCount];

	for (int i = 0; i < cxlinesCount; i++)
	{
		CvPoint* currentLine = (CvPoint*) cvGetSeqElem(lines, i);
		CvPoint* middlePoint = new CvPoint;

		middlePoint->x = (currentLine[0].x + currentLine[1].x) / 2;
		middlePoint->y = (currentLine[0].y - currentLine[1].y) / 2;
		linesMiddlePoints[i] = middlePoint;
	}

	std::vector<SpotCluster*> spots;
	std::vector<FernCluster*> ferns; // empty

	std::vector< std::vector<CvSeq*> >* clusters = new std::vector< std::vector<CvSeq*> >;

	for (int i = 0; i < cxlinesCount; i++)
	{
		if (!linesMiddlePoints[i])
			continue;

		std::vector<CvSeq*>* cluster = new std::vector<CvSeq*>;
		for (int j = i + 1; j < cxlinesCount; j++)
		{
			if (!linesMiddlePoints[j])
				continue;

			int distance = GetLineLength(linesMiddlePoints[i]->x, linesMiddlePoints[j]->x,
				linesMiddlePoints[i]->y, linesMiddlePoints[j]->y);

			if (distance < eps)
			{
				cluster->push_back((CvSeq*) cvGetSeqElem(lines, j));
				delete linesMiddlePoints[j];
				linesMiddlePoints[j] = nullptr;
			}
		}

		clusters->push_back(*cluster);
	}

	for (int i = 0; i < clusters->size(); i++)
	{
		int cxClusterSize = (*clusters)[i].size();
		if ( cxClusterSize >= minSpotLinesDencity )
		{
			CvMemStorage* memStorage = cvCreateMemStorage(0);
			CvSeq* seq = cvCreateSeq(0, sizeof(CvSeq), sizeof(CvPoint)*2, memStorage);
			for (int j = 0; j < cxClusterSize; j++)
			{
				cvSeqPush( seq, (*clusters)[i][j] );
			}
			spots.push_back(new SpotCluster(seq, cxClusterSize));
		}
		else
		{
			// TODO: Fern Detection
		}
	}
}

VOID ClusteriseLines(CvSeq* lines, LPTSTR imagePath, vector<double>& paramsPart)
{
	paramsPart.clear();
	float eps = 5;
	int minSpotLinesDencity = 10;

	DWORD cxlinesCount = lines->total;
	CvPoint* *linesMiddlePoints = new CvPoint*[cxlinesCount];

	for (int i = 0; i < cxlinesCount; i++)
	{
		CvPoint* currentLine = (CvPoint*) cvGetSeqElem(lines, i);
		CvPoint* middlePoint = new CvPoint;

		middlePoint->x = (currentLine[0].x + currentLine[1].x) / 2;
		middlePoint->y = (currentLine[0].y + currentLine[1].y) / 2;
		linesMiddlePoints[i] = middlePoint;
	}

	std::vector<SpotCluster*> spots;
	std::vector<FernCluster*> ferns; // empty

	std::vector< std::vector<CvSeq*> >* clusters = new std::vector< std::vector<CvSeq*> >;

	for (int i = 0; i < cxlinesCount; i++)
	{
		if (!linesMiddlePoints[i])
			continue;

		std::vector<CvSeq*>* cluster = new std::vector<CvSeq*>;
		for (int j = i + 1; j < cxlinesCount; j++)
		{
			if (!linesMiddlePoints[j])
				continue;

			int distance = GetLineLength(linesMiddlePoints[i]->x, linesMiddlePoints[j]->x,
				linesMiddlePoints[i]->y, linesMiddlePoints[j]->y);

			if (distance < eps)
			{
				cluster->push_back((CvSeq*) cvGetSeqElem(lines, j));
				delete linesMiddlePoints[j];
				linesMiddlePoints[j] = nullptr;
			}
		}

		clusters->push_back(*cluster);
	}

	for (int i = 0; i < clusters->size(); i++)
	{
		int cxClusterSize = (*clusters)[i].size();
		if ( cxClusterSize >= minSpotLinesDencity )
		{
			CvMemStorage* memStorage = cvCreateMemStorage(0);
			CvSeq* seq = cvCreateSeq(0, sizeof(CvSeq), sizeof(CvPoint)*2, memStorage);
			for (int j = 0; j < cxClusterSize; j++)
			{
				cvSeqPush( seq, (*clusters)[i][j] );
			}
			spots.push_back(new SpotCluster(seq, cxClusterSize));
		}
		else
		{
			// TODO: Fern Detection
		}
	}

	/* Total spots, Min radius, Max radius */
	paramsPart.push_back(spots.size());
	int maxRadius = 0;
	int minRadius = 0;

	if (spots.size() > 0)
	{
		maxRadius = spots[0]->getAverageRadius();
		minRadius = spots[0]->getAverageRadius();
		for (int i = 1; i < spots.size(); i++)
		{
			int currentRadius = spots[i]->getAverageRadius();
			if (currentRadius > maxRadius) maxRadius = currentRadius;
			if (currentRadius < minRadius) minRadius = currentRadius;
		}
	}

	paramsPart.push_back(maxRadius);
	paramsPart.push_back(maxRadius);

	/* Drawing */
	/*
	std::vector<KeyPoint> keyPoints;
	for (int i = 0; i < spots.size(); i++)
	{
		float midX = spots[i]->getAverageX();
		float midY = spots[i]->getAverageY();
		float maxDiam = spots[i]->getMaxDiametr();

		keyPoints.push_back( KeyPoint( midX, midY, maxDiam) );
	}
	
	size_t i;
	char *cv_imputFileName = new char[BUFFER_SIZE];
	wcstombs_s(&i, cv_imputFileName, BUFFER_SIZE, imagePath, BUFFER_SIZE);
	Mat im = imread(cv_imputFileName, IMREAD_COLOR);
	Mat im_with_keypoints;

	drawLines(im, lines);
	drawKeypoints(im, keyPoints, im_with_keypoints, Scalar(0, 0, 255), DrawMatchesFlags::DRAW_RICH_KEYPOINTS);
	imshow("keypoints", im_with_keypoints);
	*/
}

VOID DetectBlobs(LPTSTR imagePath)
{
	// Read image
	size_t i;
	char *cv_imputFileName = new char[BUFFER_SIZE];
	wcstombs_s(&i, cv_imputFileName, BUFFER_SIZE, imagePath, BUFFER_SIZE);
	Mat im = imread(cv_imputFileName, IMREAD_GRAYSCALE);
	 
	
	// Set up the detector with default parameters.
	SimpleBlobDetector::Params params;
	// Change thresholds
	params.minThreshold = 218;
	params.maxThreshold = 220;
	params.thresholdStep = 1;
	 
	// Filter by Area.
	params.filterByArea = true;
	params.minArea = 200;
	 
	// Filter by Circularity
	params.filterByCircularity = true;
	params.minCircularity = 0.1;
	 
	// Filter by Convexity
	params.filterByConvexity = true;
	params.minConvexity = 0.87;
	 
	// Filter by Inertia
	params.filterByInertia = true;
	params.minInertiaRatio = 0.01;

	Ptr<SimpleBlobDetector> detector = SimpleBlobDetector::create(params);
	// Detect blobs.
	std::vector<KeyPoint> keyPoints;
	try
	{
		detector->detect( im, keyPoints );
	}
	catch (Exception &cvEx)
	{
		std::cerr << cvEx.what();
	}

	// Draw detected blobs as red circles.
	// DrawMatchesFlags::DRAW_RICH_KEYPOINTS flag ensures the size of the circle corresponds to the size of blob
	Mat im_with_keypoints;
	drawKeypoints( im, keyPoints, im_with_keypoints, Scalar(0, 0, 255), DrawMatchesFlags::DRAW_RICH_KEYPOINTS );
	 
	// Show blobs
	imshow("keypoints", im_with_keypoints);
	waitKey(0);
}

VOID drawGrid(Mat& mat, int cellSize)
{
	int cxHeight = mat.rows;
	int cxWidth = mat.cols;

	for (int i = 0; i < cxHeight; i += cellSize + 1)
	{
		cv::line( mat, Point(0, i), Point(cxWidth, i), Scalar(255, 255, 255) );
	}

	for (int i = 0; i < cxWidth; i += cellSize + 1)
	{
		cv::line( mat, Point(i, 0), Point(i, cxHeight), Scalar(255, 255, 255) );
	}
}

VOID drawLines(Mat& mat, CvSeq* cvLines)
{
	for (int i = 0; i < cvLines->total; i++)
	{
		CvPoint* line = (CvPoint*) cvGetSeqElem(cvLines, i);
		cv::line( mat, Point(line[0].x, line[0].y), Point(line[1].x, line[1].y), Scalar(0, 255, 0) );
	}
}

VOID ClearImage(Mat& mat)
{
	badBlocks = 0;
	winyDensity = 100;
	winxDensity = 100;
	double multiplier = 0.8;
	int maxDensity = winyDensity * winxDensity * multiplier;
	for (int i = 0; i < mat.rows; i += winyDensity)
	{
		for (int j = 0; j < mat.cols; j += winxDensity)
		{
			int maxWinY = mat.rows - i >= winyDensity ? winyDensity : mat.rows - i;
			int maxWinX = mat.cols - j >= winxDensity ? winxDensity : mat.cols - j;
			int maxDensity = maxWinY * maxWinX * multiplier;
			int currentDensity = 0;
			for (int m = 0; m < winyDensity && i + m < mat.rows; m++)
			{
				for (int n = 0; n < winxDensity && j + n < mat.cols; n++)
				{
					unsigned char px = mat.at<unsigned char>(i + m, j + n);
					currentDensity += px == 255 ? 0 : 1;
				}
			}
			if (currentDensity >= maxDensity)
			{
				badBlocks++;
				for (int m = 0; m < winyDensity && i + m < mat.rows; m++)
				{
					for (int n = 0; n < winxDensity && j + n < mat.cols; n++)
					{
						mat.at<unsigned char>(i + m, j + n) = 255;
					}
				}
			}
		}
	}
	for (int i = 0; i < mat.rows - winyDensity; i += 1)
	{
		for (int j = 0; j < mat.cols - winxDensity; j += 1)
		{
			int maxWinY = mat.rows - i >= winyDensity ? winyDensity : mat.rows - i;
			int maxWinX = mat.cols - j >= winxDensity ? winxDensity : mat.cols - j;
			int maxDensity = maxWinY * maxWinX * multiplier;
			int currentDensity = 0;
			for (int m = 0; m < winyDensity && i + m < mat.rows; m++)
			{
				for (int n = 0; n < winxDensity && j + n < mat.cols; n++)
				{
					unsigned char px = mat.at<unsigned char>(i + m, j + n);
					currentDensity += px == 255 ? 0 : 1;
				}
			}
			if (currentDensity >= maxDensity)
			{
				badBlocks++;
				for (int m = 0; m < winyDensity && i + m < mat.rows; m++)
				{
					for (int n = 0; n < winxDensity && j + n < mat.cols; n++)
					{
						mat.at<unsigned char>(i + m, j + n) = 255;
					}
				}
			}
		}
	}
}

VOID ClearImageBorders(Mat& mat)
{
	winyDensity = 50;
	winxDensity = 50;
	double multiplier = 0.8;
	int maxDensity = winyDensity * winxDensity * multiplier;
	for (int j = 0; j < mat.cols; j += winxDensity)
	{
		int maxWinX = mat.cols - j >= winxDensity ? winxDensity : mat.cols - j;
		int maxDensity = winyDensity * maxWinX * multiplier;
		int currentDensityFirst = 0;
		int currentDensitySecond = 0;
		int currentDensityThird = 0;
		int currentDensityLast = 0;

		for (int m = 0; m < winyDensity; m++)
		{
			for (int n = 0; n < winxDensity && j + n < mat.cols; n++)
			{
				unsigned char px = mat.at<unsigned char>(m, j + n);
				currentDensityFirst += px == 255 ? 0 : 1;
				px = mat.at<unsigned char>(m + winyDensity, j + n);
				currentDensitySecond += px == 255 ? 0 : 1;
				px = mat.at<unsigned char>(mat.rows - 1 - m - winyDensity, j + n);
				currentDensityThird += px == 255 ? 0 : 1;
				px = mat.at<unsigned char>(mat.rows - 1 - m, j + n);
				currentDensityLast += px == 255 ? 0 : 1;
			}
		}

		if (currentDensityFirst >= maxDensity)
		{
			for (int m = 0; m < winyDensity; m++)
			{
				for (int n = 0; n < winxDensity && j + n < mat.cols; n++)
				{
					mat.at<unsigned char>(m, j + n) = 255;
				}
			}
		}
		if (currentDensitySecond >= maxDensity)
		{
			for (int m = 0; m < winyDensity; m++)
			{
				for (int n = 0; n < winxDensity && j + n < mat.cols; n++)
				{
					mat.at<unsigned char>(m + winyDensity, j + n) = 255;
				}
			}
		}
		if (currentDensityThird >= maxDensity)
		{
			for (int m = 0; m < winyDensity; m++)
			{
				for (int n = 0; n < winxDensity && j + n < mat.cols; n++)
				{
					mat.at<unsigned char>(mat.rows - 1 - m - winyDensity, j + n) = 255;
				}
			}
		}
		if (currentDensityLast >= maxDensity)
		{
			for (int m = 0; m < winyDensity; m++)
			{
				for (int n = 0; n < winxDensity && j + n < mat.cols; n++)
				{
					mat.at<unsigned char>(mat.rows - 1 - m, j + n) = 255;
				}
			}
		}
	}

	for (int i = 0; i < mat.rows; i += winyDensity)
	{
		int maxWinY = mat.rows - i >= winyDensity ? winyDensity : mat.rows - i;
		int maxDensity = maxWinY * winxDensity * multiplier;
		int currentDensityFirst = 0;
		int currentDensitySecond = 0;
		int currentDensityThird = 0;
		int currentDensityLast = 0;

		for (int m = 0; m < winyDensity && i + m < mat.rows; m++)
		{
			for (int n = 0; n < winxDensity; n++)
			{
				unsigned char px = mat.at<unsigned char>(i + m, n);
				currentDensityFirst += px == 255 ? 0 : 1;
				px = mat.at<unsigned char>(i + m, n + winxDensity);
				currentDensitySecond += px == 255 ? 0 : 1;
				px = mat.at<unsigned char>(i + m, mat.cols - 1 - n - winxDensity);
				currentDensityThird += px == 255 ? 0 : 1;
				px = mat.at<unsigned char>(i + m, mat.cols - 1 - n);
				currentDensityLast += px == 255 ? 0 : 1;
			}
		}

		if (currentDensityFirst >= maxDensity)
		{
			for (int m = 0; m < winyDensity && i + m < mat.rows; m++)
			{
				for (int n = 0; n < winxDensity; n++)
				{
					mat.at<unsigned char>(i + m, n) = 255;
				}
			}
		}
		if (currentDensitySecond >= maxDensity)
		{
			for (int m = 0; m < winyDensity && i + m < mat.rows; m++)
			{
				for (int n = 0; n < winxDensity; n++)
				{
					mat.at<unsigned char>(i + m, n + winxDensity) = 255;
				}
			}
		}
		if (currentDensityThird >= maxDensity)
		{
			for (int m = 0; m < winyDensity && i + m < mat.rows; m++)
			{
				for (int n = 0; n < winxDensity; n++)
				{
					mat.at<unsigned char>(i + m, mat.cols - 1 - n - winxDensity) = 255;
				}
			}
		}
		if (currentDensityLast >= maxDensity)
		{
			for (int m = 0; m < winyDensity && i + m < mat.rows; m++)
			{
				for (int n = 0; n < winxDensity; n++)
				{
					mat.at<unsigned char>(i + m, mat.cols - 1 - n) = 255;
				}
			}
		}
	}
}

int GetLineLength(int x1, int x2, int y1, int y2)
{
	return sqrt((x1 - x2)*(x1 - x2) + (y1 - y2)*(y1 - y2));
}


