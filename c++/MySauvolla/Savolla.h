#pragma once

#include "stdafx.h"

#include <opencv2\core\core.hpp>

using namespace cv;

struct LineInfo
{
	double k;
	double m;
	double length;
	double middleWidth;
};

double calcLocalStats(Mat &im, Mat &map_m, Mat &map_s, int winx, int winy);

void Sauvola(Mat im, Mat output, int winx, int winy, double k, double dR);