#pragma once
#include "cluster.h"
class FernCluster :
	public Cluster
{
private: 
	int m_mainLineLength;
	int m_subLinesAverageLength;
	std::pair<int, int> m_subLinesCount;

public:
	FernCluster(int mainLineLength, int subLineAverageLength, int leftSideCount, int rightSideCount);
	~FernCluster(void);
};

