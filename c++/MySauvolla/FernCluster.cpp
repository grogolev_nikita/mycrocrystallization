#include "stdafx.h"
#include "FernCluster.h"


FernCluster::FernCluster(int mainLineLength, int subLineAverageLength, int leftSideCount, int rightSideCount) : m_mainLineLength(mainLineLength), m_subLinesAverageLength(subLineAverageLength)
{
	m_subLinesCount.first = leftSideCount;
	m_subLinesCount.second = rightSideCount;
}


FernCluster::~FernCluster(void)
{
}
