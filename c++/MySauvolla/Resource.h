//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ generated include file.
// Used by MySavollaUI.rc
//
#define IDC_MYICON                      2
#define IDD_MYSAVOLLAUI_DIALOG          102
#define IDS_APP_TITLE                   103
#define IDD_ABOUTBOX                    103
#define IDM_ABOUT                       104
#define IDM_EXIT                        105
#define IDI_MYSAVOLLAUI                 107
#define IDI_SMALL                       108
#define IDC_MYSAVOLLAUI                 109
#define IDR_MAINFRAME                   128
#define ID_FILE_OPENFILE                1000
#define IDC_BINARIZE_BUTTON             1001
#define IDC_GET_LINES_BUTTON            1002
#define IDC_CLEAR_LINES_BUTTON          1003
#define IDC_GET_FERNS_BUTTON            1005
#define IDC_FULL_IMAGE_ANALYSIS			1006
#define IDC_CANNY_CHECKBOX				1004
#define IDC_STATIC                      -1

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NO_MFC                     1
#define _APS_NEXT_RESOURCE_VALUE        129
#define _APS_NEXT_COMMAND_VALUE         32772
#define _APS_NEXT_CONTROL_VALUE         1000
#define _APS_NEXT_SYMED_VALUE           110
#endif
#endif
