#include "stdafx.h"
#include "CSVDataExporter.h"
#include <fstream>

CSVDataExporter::CSVDataExporter(const char* fileName, char* headers[], int headersCount)
{
	m_csvFile.open(fileName, std::fstream::out | std::fstream::app);
	for (int i = 0; i < headersCount; i++)
	{
		m_csvFile << headers[i] << ';';
	}
	m_csvFile << std::endl;
}

CSVDataExporter::~CSVDataExporter(void)
{
	m_csvFile.close();
}